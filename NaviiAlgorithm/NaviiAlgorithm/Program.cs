﻿using Persistence.Models.USDA;
using Persistence.ProtoNaviDbContext;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace NaviiAlgorithm
{
    class Program
    {
        private const bool Debug = false;

        private static USDAContext _usdaContext;

        private class ComboStatistics
        {
            public decimal Calories;
            public decimal Protein;
            public decimal Fat;
            public decimal Carbs;
        }

        private class FoodCombination
        {
            public List<FoodSelection> FoodList;
            public ComboStatistics ComboStats;
        }

        private class FoodSelection
        {
            public FoodDes Food;
            public decimal Amount;
            public Weight Weight;
            public decimal Calories;
            public decimal Protein;
            public decimal Fat;
            public decimal Carbs;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("Press any key to begin, or press X to exit.");
            var key = Console.ReadKey(true);
            if (key.Key.ToString().ToUpper() == "X")
                Process.GetCurrentProcess().Kill();
            Console.WriteLine("");

            _usdaContext = new USDAContext();

            // Targets and Limits
            const int combosTarget = 1000;
            const int maxComboLoops = 10000;
            const int maxFoodLoops = 75;

            // Nutrition Plan Definition
            const decimal minCalories = 750m;
            const decimal maxCalories = 900m;
            const decimal minProtein = 24m;
            const decimal minFat = 34m;
            const decimal minCarbs = 75m;
            const decimal maxProtein = 26m;
            const decimal maxFat = 39m;
            const decimal maxCarbs = 90m;

            // Food Group Weight Amounts
            var groupWeights = new Dictionary<string, int>
            {
                { "0100", 0 },
                { "0200", 0 },
                { "0300", 0 },
                { "0400", 0 },
                { "0500", 0 },
                { "0600", 0 },
                { "0700", 0 },
                { "0800", 0 },
                { "0900", 0 },
                { "1000", 0 },
                { "1100", 0 },
                { "1200", 0 },
                { "1300", 0 },
                { "1400", 0 },
                { "1500", 0 },
                { "1600", 0 },
                { "1700", 0 },
                { "1800", 0 },
                { "1900", 0 },
                { "2000", 0 },
                { "2100", 0 },
                { "2200", 0 },
                { "2500", 0 },
                { "3500", 0 },
                { "3600", 0 },
            };

            var foods = _usdaContext.FoodDes.Where(f => f.IsActive).ToList();

            Console.WriteLine("Initiating algorithm:");
            Console.WriteLine(" --- Target Combinations: {0}", combosTarget);
            Console.WriteLine(" --- Combination Attempts: {0}", maxComboLoops);
            Console.WriteLine(" --- Food Attempts: {0}", maxFoodLoops);
            Console.WriteLine(" --- Allowable Calories: {0}-{1}kcal", minCalories, maxCalories);
            Console.WriteLine(" --- Allowable Protein: {0}-{1}g", minProtein, maxProtein);
            Console.WriteLine(" --- Allowable Fat: {0}-{1}g", minFat, maxFat);
            Console.WriteLine(" --- Allowable Carbs: {0}-{1}g", minCarbs, maxCarbs);
            Console.WriteLine("");
            Console.WriteLine("");

            // Try a given number of times to populate 100 valid combos
            var validCombos = new List<FoodCombination>();
            var comboLoops = 0;
            while (comboLoops < maxComboLoops && validCombos.Count < combosTarget)
            {
                Console.WriteLine("\r >>> Combo Iteration: {0} | Valid Combinations: {1}", comboLoops+1, validCombos.Count);
                var combo = new List<FoodSelection>();
                var comboStats = new ComboStatistics();

                // Try a given number of times to add foods to a combo, and stop when you get a valid combo or break the limits
                var foodLoops = 0;
                while (foodLoops < maxFoodLoops)
                {
                    if (comboStats.Calories >= minCalories && comboStats.Calories <= maxCalories &&
                        comboStats.Protein >= minProtein && comboStats.Protein <= maxProtein &&
                        comboStats.Fat >= minFat && comboStats.Fat <= maxFat &&
                        comboStats.Carbs >= minCarbs && comboStats.Carbs <= maxCarbs)
                    {
                        validCombos.Add(new FoodCombination { FoodList = combo, ComboStats = comboStats });
                        Console.WriteLine("\n********************************************************************************");
                        Console.WriteLine("Added valid combo with {0} food items totaling {4} calories, {1}g protein, {2}g fat, and {3}g carbs",
                            combo.Count, comboStats.Protein, comboStats.Fat, comboStats.Carbs, comboStats.Calories);
                        Console.WriteLine("********************************************************************************");
                        Console.WriteLine("");
                        break;
                    }

                    Console.Write("\r --- Food Iteration: {0}", foodLoops+1);
                    if (Debug) Console.WriteLine("\nThe current combo has {0} food items totaling {1}g protein, {2}g fat, and {3}g carbs",
                        combo.Count, comboStats.Protein, comboStats.Fat, comboStats.Carbs);
                    if (Debug) Console.WriteLine("Limits have not been reached, so adding another food.");
                    if (Debug) Console.WriteLine("--------------------------------------------------------------------------------");
                    if (Debug) Console.WriteLine("");
                    if (Debug) Console.WriteLine("");

                    // Initialize a new random number generator each iteration of this loop 
                    var random = new Random();
                    
                    // Get a random food and if you don't find one, skip this iteration
                    var randomFoodNumber = random.Next(0, foods.Count-1);
                    var randomFood = foods[randomFoodNumber];
                    if (randomFood is null)
                    {
                        if (Debug) Console.WriteLine("Can't find a food for the random number selected.", randomFoodNumber);
                        if (Debug) Console.WriteLine("--------------------------------------------------------------------------------");
                        if (Debug) Console.WriteLine("");
                        if (Debug) Console.WriteLine("");
                        continue;
                    }

                    if (Debug) Console.WriteLine("Chose food {0}.", randomFood.ShrtDesc);

                    // Get a random weight measure from the food and if you don't find one, skip this iteration
                    var weights = _usdaContext.Weight.Where(w => w.NdbNo == randomFood.NdbNo).ToList();
                    if (weights.Count == 0)
                    {
                        if (Debug) Console.WriteLine("Can't find a weight for this food.", randomFoodNumber);
                        if (Debug) Console.WriteLine("--------------------------------------------------------------------------------");
                        if (Debug) Console.WriteLine("");
                        if (Debug) Console.WriteLine("");
                        continue;
                    }
                    var randomWeightNumber = random.Next(0, weights.Count-1);
                    var randomWeight = weights[randomWeightNumber];
                    if (randomWeight is null)
                    {
                        if (Debug) Console.WriteLine("Can't find a food weight for the random number selected.", randomFoodNumber);
                        if (Debug) Console.WriteLine("--------------------------------------------------------------------------------");
                        if (Debug) Console.WriteLine("");
                        if (Debug) Console.WriteLine("");
                        continue;
                    }
                    if (Debug) Console.WriteLine("Chose measurement type {0} ({1}g).", randomWeight.MsreDesc, randomWeight.GmWgt);

                    // Get a random integer multiplier for the number of units of the random weight selected
                    var randomAmount =
                        randomWeight.GmWgt > 400m ? 0.125m :
                        randomWeight.GmWgt > 200m ? 0.25m :
                        randomWeight.GmWgt > 100m ? 0.5m :
                        randomWeight.GmWgt >  50m ? 1 :
                        random.Next(1, Convert.ToInt32(Math.Floor(100 / randomWeight.GmWgt)));
                    if (Debug) Console.WriteLine("Chose to use {0} {1} of this food.", randomAmount, randomWeight.MsreDesc);

                    // Determine the amount of protein, fat and carbs in the food and skip this iteration if any are null
                    var foodCalories = _usdaContext.NutData?.FirstOrDefault(n => n.NdbNo == randomFood.NdbNo && n.NutrNo == "208")?.NutrVal ?? 99999m;
                    var foodProtein = _usdaContext.NutData?.FirstOrDefault(n => n.NdbNo == randomFood.NdbNo && n.NutrNo == "203")?.NutrVal ?? 99999m;
                    var foodFat = _usdaContext.NutData?.FirstOrDefault(n => n.NdbNo == randomFood.NdbNo && n.NutrNo == "204")?.NutrVal ?? 99999m;
                    var foodCarbs = _usdaContext.NutData?.FirstOrDefault(n => n.NdbNo == randomFood.NdbNo && n.NutrNo == "205")?.NutrVal ?? 99999m;
                    if (foodProtein == 99999m || foodFat == 99999m || foodCarbs == 99999m || foodCalories == 99999m)
                    {
                        if (Debug) Console.WriteLine("Unable to determine the amount of calories, protein, fat or carbs in 100g of this food.");
                        if (Debug) Console.WriteLine("--------------------------------------------------------------------------------");
                        if (Debug) Console.WriteLine("");
                        if (Debug) Console.WriteLine("");
                        continue;
                    }

                    var amountCalories = foodCalories * (randomAmount * randomWeight.GmWgt) / 100;
                    var amountProtein = foodProtein * (randomAmount * randomWeight.GmWgt) / 100;
                    var amountFat = foodFat * (randomAmount * randomWeight.GmWgt) / 100;
                    var amountCarbs = foodCarbs * (randomAmount * randomWeight.GmWgt) / 100;
                    if (Debug) Console.WriteLine("This amount of food has {0}g protein, {1}g fat, and {2}g carbs.", amountProtein, amountFat, amountCarbs);

                    if (// This amount of food itself is within limits
                        amountCalories < maxCalories && amountProtein < maxProtein && amountFat < maxFat && amountCarbs < maxCarbs &&
                        // This amount of food doesn't add too much to the existing combo
                        amountCalories + comboStats.Calories < maxCalories &&
                        amountProtein + comboStats.Protein < maxProtein &&
                        amountFat + comboStats.Fat < maxFat &&
                        amountCarbs + comboStats.Carbs < maxCarbs)
                    {
                        combo.Add(new FoodSelection
                        {
                            Food = randomFood,
                            Amount = randomAmount,
                            Weight = randomWeight,
                            Calories = amountCalories,
                            Protein = amountProtein,
                            Fat = amountFat,
                            Carbs = amountCarbs,
                        });
                        comboStats.Calories += amountCalories;
                        comboStats.Protein += amountProtein;
                        comboStats.Fat += amountFat;
                        comboStats.Carbs += amountCarbs;

                        if (Debug) Console.WriteLine("Still within tolerance! Let's continue...");
                        if (Debug) Console.WriteLine("--------------------------------------------------------------------------------");
                        if (Debug) Console.WriteLine("");
                        if (Debug) Console.WriteLine("");
                    }
                    else
                    {
                        if (Debug) Console.WriteLine("Unfortunately, this food added too much to the combo, so let's start over.");
                        if (Debug) Console.WriteLine("--------------------------------------------------------------------------------");
                        if (Debug) Console.WriteLine("");
                        if (Debug) Console.WriteLine("");
                    }
                    foodLoops++;
                }
                comboLoops++;
            }

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(
                $@"C:\RGP\navi\Data\Combos\{DateTime.UtcNow.Year}-{DateTime.UtcNow.Month}-{DateTime.UtcNow.Day}-{DateTime.UtcNow.Hour}-{DateTime.UtcNow.Minute}-{DateTime.UtcNow.Second}.txt"))
            {
                foreach (FoodCombination combo in validCombos)
                {
                    file.WriteLine("---------------------------------------");
                    file.WriteLine("Food combination with {0} items:", combo.FoodList.Count);
                    file.WriteLine(" --- {0} Calories", combo.ComboStats.Calories);
                    file.WriteLine(" --- {0}g Protein", combo.ComboStats.Protein);
                    file.WriteLine(" --- {0}g Fat", combo.ComboStats.Fat);
                    file.WriteLine(" --- {0}g Carbs", combo.ComboStats.Carbs);
                    file.WriteLine("");

                    foreach (FoodSelection food in combo.FoodList)
                    {
                        file.WriteLine(" > {0} {1} {2}", food.Amount, food.Weight.MsreDesc, food.Food.ShrtDesc);
                    }
                }
            }

            using (System.IO.StreamWriter file = new System.IO.StreamWriter(
                $@"C:\RGP\navi\Data\Combos\{DateTime.UtcNow.Year}-{DateTime.UtcNow.Month}-{DateTime.UtcNow.Day}-{DateTime.UtcNow.Hour}-{DateTime.UtcNow.Minute}-{DateTime.UtcNow.Second}_formatted.txt"))
            {
                file.WriteLine("Combo|Combo Calories (kcal)|Combo Proten (g)|Combo Fat (g)|Combo Carbs (g)|Food Amount|Food Unit|Food Description|Food Calories (kcal)|Food Proten (g)|Food Fat (g)|Food Carbs (g)");
                var comboNumber = 0;
                foreach (FoodCombination combo in validCombos)
                {
                    foreach (FoodSelection food in combo.FoodList)
                    {
                        file.WriteLine("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}|{11}",
                            comboNumber, combo.ComboStats.Calories, combo.ComboStats.Protein, combo.ComboStats.Fat, combo.ComboStats.Carbs,
                            food.Amount, food.Weight.MsreDesc, food.Food.ShrtDesc,
                            food.Calories, food.Protein, food.Fat, food.Carbs);
                    }

                    comboNumber++;
                }
            }

            Console.WriteLine("\n================================================================================");
            Console.WriteLine("Number of valid combos: {0}", validCombos.Count);
            Console.WriteLine("================================================================================");
            Console.WriteLine("Press any key to exit.");
            Console.ReadKey(true);
        }
    }
}
