﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class Datsrcln
    {
        public string NdbNo { get; set; }
        public string NutrNo { get; set; }
        public string DataSrcId { get; set; }

        public DataSrc DataSrc { get; set; }
        public FoodDes NdbNoNavigation { get; set; }
        public NutrDef NutrNoNavigation { get; set; }
    }
}
