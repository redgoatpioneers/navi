﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class DerivCd
    {
        public DerivCd()
        {
            NutData = new HashSet<NutData>();
        }

        public string DerivCd1 { get; set; }
        public string DerivDesc { get; set; }

        public ICollection<NutData> NutData { get; set; }
    }
}
