﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class FdGroup
    {
        public FdGroup()
        {
            FoodDes = new HashSet<FoodDes>();
        }

        public string FdGrpCd { get; set; }
        public string FdGrpDesc { get; set; }

        public ICollection<FoodDes> FoodDes { get; set; }
    }
}
