﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class FoodDes
    {
        public FoodDes()
        {
            Datsrcln = new HashSet<Datsrcln>();
            Footnote = new HashSet<Footnote>();
            Langual = new HashSet<Langual>();
            NutData = new HashSet<NutData>();
            Weight = new HashSet<Weight>();
        }

        public string NdbNo { get; set; }
        public string FdGrpCd { get; set; }
        public string LongDesc { get; set; }
        public string ShrtDesc { get; set; }
        public string ComName { get; set; }
        public string ManufacName { get; set; }
        public string Survey { get; set; }
        public string RefDesc { get; set; }
        public string Refuse { get; set; }
        public string SciName { get; set; }
        public decimal? NFactor { get; set; }
        public decimal? ProFactor { get; set; }
        public decimal? FatFactor { get; set; }
        public decimal? ChoFactor { get; set; }
        public bool IsActive { get; set; }

        public FdGroup FdGrpCdNavigation { get; set; }
        public ICollection<Datsrcln> Datsrcln { get; set; }
        public ICollection<Footnote> Footnote { get; set; }
        public ICollection<Langual> Langual { get; set; }
        public ICollection<NutData> NutData { get; set; }
        public ICollection<Weight> Weight { get; set; }
    }
}
