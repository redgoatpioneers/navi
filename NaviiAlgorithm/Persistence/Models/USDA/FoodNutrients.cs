﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class FoodNutrients
    {
        public int FoodNutrientId { get; set; }
        public decimal Amount { get; set; }
        public int FoodId { get; set; }
        public int NutrientTypeId { get; set; }

        public Foods Food { get; set; }
        public NutrientTypes NutrientType { get; set; }
    }
}
