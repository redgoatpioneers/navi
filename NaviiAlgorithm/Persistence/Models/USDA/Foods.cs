﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class Foods
    {
        public Foods()
        {
            FoodNutrients = new HashSet<FoodNutrients>();
            RecipeFoods = new HashSet<RecipeFoods>();
        }

        public int FoodId { get; set; }
        public string Description { get; set; }
        public string ImageThumbnailUrl { get; set; }
        public string Name { get; set; }
        public bool IsActive { get; set; }
        public decimal Price { get; set; }

        public ICollection<FoodNutrients> FoodNutrients { get; set; }
        public ICollection<RecipeFoods> RecipeFoods { get; set; }
    }
}
