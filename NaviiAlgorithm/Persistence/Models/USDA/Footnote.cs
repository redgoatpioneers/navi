﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class Footnote
    {
        public string NdbNo { get; set; }
        public string FootntNo { get; set; }
        public string FootntTyp { get; set; }
        public string NutrNo { get; set; }
        public string FootntTxt { get; set; }

        public FoodDes NdbNoNavigation { get; set; }
        public NutrDef NutrNoNavigation { get; set; }
    }
}
