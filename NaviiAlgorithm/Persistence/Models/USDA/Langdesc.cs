﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class Langdesc
    {
        public Langdesc()
        {
            Langual = new HashSet<Langual>();
        }

        public string FactorCode { get; set; }
        public string Description { get; set; }

        public ICollection<Langual> Langual { get; set; }
    }
}
