﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class Langual
    {
        public string NdbNo { get; set; }
        public string FactorCode { get; set; }

        public Langdesc FactorCodeNavigation { get; set; }
        public FoodDes NdbNoNavigation { get; set; }
    }
}
