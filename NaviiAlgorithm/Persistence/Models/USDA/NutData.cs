﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class NutData
    {
        public string NdbNo { get; set; }
        public string NutrNo { get; set; }
        public decimal NutrVal { get; set; }
        public decimal NumDataPts { get; set; }
        public decimal? StdError { get; set; }
        public string SrcCd { get; set; }
        public string DerivCd { get; set; }
        public string RefNdbNo { get; set; }
        public string AddNutrMark { get; set; }
        public decimal? NumStudies { get; set; }
        public decimal? Min { get; set; }
        public decimal? Max { get; set; }
        public decimal? Df { get; set; }
        public decimal? LowEb { get; set; }
        public decimal? UpEb { get; set; }
        public string StatCmt { get; set; }
        public string AddModDate { get; set; }

        public DerivCd DerivCdNavigation { get; set; }
        public FoodDes NdbNoNavigation { get; set; }
        public NutrDef NutrNoNavigation { get; set; }
        public SrcCd SrcCdNavigation { get; set; }
    }
}
