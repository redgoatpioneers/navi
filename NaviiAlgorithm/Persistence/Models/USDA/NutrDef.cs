﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class NutrDef
    {
        public NutrDef()
        {
            Datsrcln = new HashSet<Datsrcln>();
            Footnote = new HashSet<Footnote>();
            NutData = new HashSet<NutData>();
        }

        public string NutrNo { get; set; }
        public string Units { get; set; }
        public string Tagname { get; set; }
        public string NutrDesc { get; set; }
        public string NumDec { get; set; }
        public decimal? SrOrder { get; set; }

        public NutrDef NutrNoNavigation { get; set; }
        public NutrDef InverseNutrNoNavigation { get; set; }
        public ICollection<Datsrcln> Datsrcln { get; set; }
        public ICollection<Footnote> Footnote { get; set; }
        public ICollection<NutData> NutData { get; set; }
    }
}
