﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class NutrientTypes
    {
        public NutrientTypes()
        {
            FoodNutrients = new HashSet<FoodNutrients>();
        }

        public int NutrientTypeId { get; set; }
        public string Description { get; set; }
        public string Name { get; set; }
        public string Unit { get; set; }
        public int Category { get; set; }

        public ICollection<FoodNutrients> FoodNutrients { get; set; }
    }
}
