﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class RecipeFoods
    {
        public int RecipeFoodId { get; set; }
        public decimal Amount { get; set; }
        public int FoodId { get; set; }
        public int RecipeId { get; set; }
        public decimal RecipeQuantity { get; set; }
        public string RecipeUnit { get; set; }

        public Foods Food { get; set; }
        public Recipies Recipe { get; set; }
    }
}
