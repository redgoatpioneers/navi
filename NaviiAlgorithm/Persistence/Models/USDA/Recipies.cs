﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class Recipies
    {
        public Recipies()
        {
            RecipeFoods = new HashSet<RecipeFoods>();
        }

        public int RecipeId { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }
        public string ImageThumbnailUrl { get; set; }
        public string Instructions { get; set; }
        public bool IsActive { get; set; }
        public string Name { get; set; }
        public string Source { get; set; }
        public string Type { get; set; }

        public ICollection<RecipeFoods> RecipeFoods { get; set; }
    }
}
