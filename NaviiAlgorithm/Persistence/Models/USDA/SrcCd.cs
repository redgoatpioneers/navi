﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class SrcCd
    {
        public SrcCd()
        {
            NutData = new HashSet<NutData>();
        }

        public string SrcCd1 { get; set; }
        public string SrcCdDesc { get; set; }

        public ICollection<NutData> NutData { get; set; }
    }
}
