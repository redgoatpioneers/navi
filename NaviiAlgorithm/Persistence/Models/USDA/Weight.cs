﻿using System;
using System.Collections.Generic;

namespace Persistence.Models.USDA
{
    public partial class Weight
    {
        public string NdbNo { get; set; }
        public string Seq { get; set; }
        public decimal Amount { get; set; }
        public string MsreDesc { get; set; }
        public decimal GmWgt { get; set; }
        public decimal? NumDataPts { get; set; }
        public decimal? StdDev { get; set; }

        public FoodDes NdbNoNavigation { get; set; }
    }
}
