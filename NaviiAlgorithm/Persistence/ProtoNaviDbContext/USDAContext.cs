﻿using Microsoft.EntityFrameworkCore;
using Persistence.Models.USDA;

namespace Persistence.ProtoNaviDbContext
{
    public partial class USDAContext : DbContext
    {
        public USDAContext()
        {
        }

        public USDAContext(DbContextOptions<USDAContext> options)
            : base(options)
        {
        }

        public virtual DbSet<DataSrc> DataSrc { get; set; }
        public virtual DbSet<Datsrcln> Datsrcln { get; set; }
        public virtual DbSet<DerivCd> DerivCd { get; set; }
        public virtual DbSet<FdGroup> FdGroup { get; set; }
        public virtual DbSet<FoodDes> FoodDes { get; set; }
        public virtual DbSet<FoodNutrients> FoodNutrients { get; set; }
        public virtual DbSet<Foods> Foods { get; set; }
        public virtual DbSet<Footnote> Footnote { get; set; }
        public virtual DbSet<Langdesc> Langdesc { get; set; }
        public virtual DbSet<Langual> Langual { get; set; }
        public virtual DbSet<NutData> NutData { get; set; }
        public virtual DbSet<NutrDef> NutrDef { get; set; }
        public virtual DbSet<NutrientTypes> NutrientTypes { get; set; }
        public virtual DbSet<RecipeFoods> RecipeFoods { get; set; }
        public virtual DbSet<Recipies> Recipies { get; set; }
        public virtual DbSet<SrcCd> SrcCd { get; set; }
        public virtual DbSet<Weight> Weight { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
                optionsBuilder.UseSqlServer("Server=(localdb)\\MSSQLLocalDB;Database=ProtoNaviDb;Trusted_Connection=True;");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<DataSrc>(entity =>
            {
                entity.ToTable("DATA_SRC", "usda");

                entity.Property(e => e.DataSrcId)
                    .HasColumnName("DataSrc_ID")
                    .HasMaxLength(6)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Authors)
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.EndPage)
                    .HasColumnName("End_Page")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.IssueState)
                    .HasColumnName("Issue_State")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Journal)
                    .HasMaxLength(135)
                    .IsUnicode(false);

                entity.Property(e => e.StartPage)
                    .HasColumnName("Start_Page")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Title)
                    .IsRequired()
                    .HasMaxLength(255)
                    .IsUnicode(false);

                entity.Property(e => e.VolCity)
                    .HasColumnName("Vol_City")
                    .HasMaxLength(16)
                    .IsUnicode(false);

                entity.Property(e => e.Year)
                    .HasMaxLength(4)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Datsrcln>(entity =>
            {
                entity.HasKey(e => new { e.NdbNo, e.NutrNo, e.DataSrcId });

                entity.ToTable("DATSRCLN", "usda");

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.NutrNo)
                    .HasColumnName("Nutr_No")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.DataSrcId)
                    .HasColumnName("DataSrc_ID")
                    .HasMaxLength(6)
                    .IsUnicode(false);

                entity.HasOne(d => d.DataSrc)
                    .WithMany(p => p.Datsrcln)
                    .HasForeignKey(d => d.DataSrcId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DATSRCLN__DataSr__44CA3770");

                entity.HasOne(d => d.NdbNoNavigation)
                    .WithMany(p => p.Datsrcln)
                    .HasForeignKey(d => d.NdbNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DATSRCLN__NDB_No__42E1EEFE");

                entity.HasOne(d => d.NutrNoNavigation)
                    .WithMany(p => p.Datsrcln)
                    .HasForeignKey(d => d.NutrNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__DATSRCLN__Nutr_N__43D61337");
            });

            modelBuilder.Entity<DerivCd>(entity =>
            {
                entity.HasKey(e => e.DerivCd1);

                entity.ToTable("DERIV_CD", "usda");

                entity.Property(e => e.DerivCd1)
                    .HasColumnName("Deriv_Cd")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.DerivDesc)
                    .IsRequired()
                    .HasColumnName("Deriv_Desc")
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FdGroup>(entity =>
            {
                entity.HasKey(e => e.FdGrpCd);

                entity.ToTable("FD_GROUP", "usda");

                entity.Property(e => e.FdGrpCd)
                    .HasColumnName("FdGrp_Cd")
                    .HasMaxLength(4)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.FdGrpDesc)
                    .IsRequired()
                    .HasColumnName("FdGrp_Desc")
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<FoodDes>(entity =>
            {
                entity.HasKey(e => e.NdbNo);

                entity.ToTable("FOOD_DES", "usda");

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.ChoFactor)
                    .HasColumnName("CHO_Factor")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.ComName)
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.FatFactor)
                    .HasColumnName("Fat_Factor")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.FdGrpCd)
                    .HasColumnName("FdGrp_Cd")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.LongDesc)
                    .HasColumnName("Long_Desc")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.ManufacName)
                    .HasMaxLength(65)
                    .IsUnicode(false);

                entity.Property(e => e.NFactor)
                    .HasColumnName("N_Factor")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.ProFactor)
                    .HasColumnName("Pro_Factor")
                    .HasColumnType("decimal(4, 2)");

                entity.Property(e => e.RefDesc)
                    .HasColumnName("Ref_desc")
                    .HasMaxLength(135)
                    .IsUnicode(false);

                entity.Property(e => e.Refuse)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.SciName)
                    .HasMaxLength(65)
                    .IsUnicode(false);

                entity.Property(e => e.ShrtDesc)
                    .HasColumnName("Shrt_Desc")
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.Survey)
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.FdGrpCdNavigation)
                    .WithMany(p => p.FoodDes)
                    .HasForeignKey(d => d.FdGrpCd)
                    .HasConstraintName("FK__FOOD_DES__FdGrp___45BE5BA9");
            });

            modelBuilder.Entity<FoodNutrients>(entity =>
            {
                entity.HasKey(e => e.FoodNutrientId);

                entity.HasIndex(e => e.FoodId)
                    .HasName("IX_Nutrients_FoodId");

                entity.HasIndex(e => e.NutrientTypeId)
                    .HasName("IX_Nutrients_NutrientTypeId");

                entity.HasOne(d => d.Food)
                    .WithMany(p => p.FoodNutrients)
                    .HasForeignKey(d => d.FoodId)
                    .HasConstraintName("FK_Nutrients_Foods_FoodId");

                entity.HasOne(d => d.NutrientType)
                    .WithMany(p => p.FoodNutrients)
                    .HasForeignKey(d => d.NutrientTypeId)
                    .HasConstraintName("FK_Nutrients_NutrientTypes_NutrientTypeId");
            });

            modelBuilder.Entity<Foods>(entity =>
            {
                entity.HasKey(e => e.FoodId);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.ImageThumbnailUrl).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Price).HasColumnType("money");
            });

            modelBuilder.Entity<Footnote>(entity =>
            {
                entity.HasKey(e => new { e.NdbNo, e.FootntNo, e.NutrNo });

                entity.ToTable("FOOTNOTE", "usda");

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.FootntNo)
                    .HasColumnName("Footnt_No")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.NutrNo)
                    .HasColumnName("Nutr_No")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.FootntTxt)
                    .HasColumnName("Footnt_Txt")
                    .HasMaxLength(200)
                    .IsUnicode(false);

                entity.Property(e => e.FootntTyp)
                    .IsRequired()
                    .HasColumnName("Footnt_Typ")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.HasOne(d => d.NdbNoNavigation)
                    .WithMany(p => p.Footnote)
                    .HasForeignKey(d => d.NdbNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__FOOTNOTE__NDB_No__46B27FE2");

                entity.HasOne(d => d.NutrNoNavigation)
                    .WithMany(p => p.Footnote)
                    .HasForeignKey(d => d.NutrNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__FOOTNOTE__Nutr_N__4F47C5E3");
            });

            modelBuilder.Entity<Langdesc>(entity =>
            {
                entity.HasKey(e => e.FactorCode);

                entity.ToTable("LANGDESC", "usda");

                entity.Property(e => e.FactorCode)
                    .HasColumnName("Factor_Code")
                    .HasMaxLength(5)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(140)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Langual>(entity =>
            {
                entity.HasKey(e => new { e.NdbNo, e.FactorCode });

                entity.ToTable("LANGUAL", "usda");

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.FactorCode)
                    .HasColumnName("Factor_Code")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.HasOne(d => d.FactorCodeNavigation)
                    .WithMany(p => p.Langual)
                    .HasForeignKey(d => d.FactorCode)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LANGUAL__Factor___489AC854");

                entity.HasOne(d => d.NdbNoNavigation)
                    .WithMany(p => p.Langual)
                    .HasForeignKey(d => d.NdbNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__LANGUAL__NDB_No__47A6A41B");
            });

            modelBuilder.Entity<NutData>(entity =>
            {
                entity.HasKey(e => new { e.NdbNo, e.NutrNo });

                entity.ToTable("NUT_DATA", "usda");

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.NutrNo)
                    .HasColumnName("Nutr_No")
                    .HasMaxLength(3)
                    .IsUnicode(false);

                entity.Property(e => e.AddModDate)
                    .HasColumnName("AddMod_Date")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.AddNutrMark)
                    .HasColumnName("Add_Nutr_Mark")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.DerivCd)
                    .HasColumnName("Deriv_Cd")
                    .HasMaxLength(4)
                    .IsUnicode(false);

                entity.Property(e => e.Df)
                    .HasColumnName("DF")
                    .HasColumnType("decimal(4, 0)");

                entity.Property(e => e.LowEb)
                    .HasColumnName("Low_EB")
                    .HasColumnType("decimal(10, 3)");

                entity.Property(e => e.Max).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.Min).HasColumnType("decimal(10, 3)");

                entity.Property(e => e.NumDataPts)
                    .HasColumnName("Num_Data_Pts")
                    .HasColumnType("decimal(5, 0)");

                entity.Property(e => e.NumStudies)
                    .HasColumnName("Num_Studies")
                    .HasColumnType("decimal(2, 0)");

                entity.Property(e => e.NutrVal)
                    .HasColumnName("Nutr_Val")
                    .HasColumnType("decimal(10, 3)");

                entity.Property(e => e.RefNdbNo)
                    .HasColumnName("Ref_NDB_No")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.SrcCd)
                    .HasColumnName("Src_Cd")
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.StatCmt)
                    .HasColumnName("Stat_cmt")
                    .HasMaxLength(10)
                    .IsUnicode(false);

                entity.Property(e => e.StdError)
                    .HasColumnName("Std_Error")
                    .HasColumnType("decimal(8, 3)");

                entity.Property(e => e.UpEb)
                    .HasColumnName("Up_EB")
                    .HasColumnType("decimal(10, 3)");

                entity.HasOne(d => d.DerivCdNavigation)
                    .WithMany(p => p.NutData)
                    .HasForeignKey(d => d.DerivCd)
                    .HasConstraintName("FK__NUT_DATA__Deriv___4C6B5938");

                entity.HasOne(d => d.NdbNoNavigation)
                    .WithMany(p => p.NutData)
                    .HasForeignKey(d => d.NdbNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__NUT_DATA__NDB_No__498EEC8D");

                entity.HasOne(d => d.NutrNoNavigation)
                    .WithMany(p => p.NutData)
                    .HasForeignKey(d => d.NutrNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__NUT_DATA__Nutr_N__4A8310C6");

                entity.HasOne(d => d.SrcCdNavigation)
                    .WithMany(p => p.NutData)
                    .HasForeignKey(d => d.SrcCd)
                    .HasConstraintName("FK__NUT_DATA__Src_Cd__4B7734FF");
            });

            modelBuilder.Entity<NutrDef>(entity =>
            {
                entity.HasKey(e => e.NutrNo);

                entity.ToTable("NUTR_DEF", "usda");

                entity.Property(e => e.NutrNo)
                    .HasColumnName("Nutr_No")
                    .HasMaxLength(3)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.NumDec)
                    .HasColumnName("Num_Dec")
                    .HasMaxLength(1)
                    .IsUnicode(false);

                entity.Property(e => e.NutrDesc)
                    .HasMaxLength(60)
                    .IsUnicode(false);

                entity.Property(e => e.SrOrder)
                    .HasColumnName("SR_Order")
                    .HasColumnType("decimal(6, 0)");

                entity.Property(e => e.Tagname)
                    .HasMaxLength(20)
                    .IsUnicode(false);

                entity.Property(e => e.Units)
                    .IsRequired()
                    .HasMaxLength(7)
                    .IsUnicode(false);

                entity.HasOne(d => d.NutrNoNavigation)
                    .WithOne(p => p.InverseNutrNoNavigation)
                    .HasForeignKey<NutrDef>(d => d.NutrNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__NUTR_DEF__Nutr_N__4D5F7D71");
            });

            modelBuilder.Entity<NutrientTypes>(entity =>
            {
                entity.HasKey(e => e.NutrientTypeId);
            });

            modelBuilder.Entity<RecipeFoods>(entity =>
            {
                entity.HasKey(e => e.RecipeFoodId);

                entity.HasIndex(e => e.FoodId);

                entity.HasIndex(e => e.RecipeId);

                entity.HasOne(d => d.Food)
                    .WithMany(p => p.RecipeFoods)
                    .HasForeignKey(d => d.FoodId);

                entity.HasOne(d => d.Recipe)
                    .WithMany(p => p.RecipeFoods)
                    .HasForeignKey(d => d.RecipeId);
            });

            modelBuilder.Entity<Recipies>(entity =>
            {
                entity.HasKey(e => e.RecipeId);

                entity.Property(e => e.Description)
                    .IsRequired()
                    .HasMaxLength(300);

                entity.Property(e => e.ImageThumbnailUrl).HasMaxLength(100);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.Source).HasMaxLength(300);
            });

            modelBuilder.Entity<SrcCd>(entity =>
            {
                entity.HasKey(e => e.SrcCd1);

                entity.ToTable("SRC_CD", "usda");

                entity.Property(e => e.SrcCd1)
                    .HasColumnName("SRC_Cd")
                    .HasMaxLength(2)
                    .IsUnicode(false)
                    .ValueGeneratedNever();

                entity.Property(e => e.SrcCdDesc)
                    .IsRequired()
                    .HasColumnName("SrcCd_Desc")
                    .HasMaxLength(60)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Weight>(entity =>
            {
                entity.HasKey(e => new { e.NdbNo, e.Seq });

                entity.ToTable("WEIGHT", "usda");

                entity.Property(e => e.NdbNo)
                    .HasColumnName("NDB_No")
                    .HasMaxLength(5)
                    .IsUnicode(false);

                entity.Property(e => e.Seq)
                    .HasMaxLength(2)
                    .IsUnicode(false);

                entity.Property(e => e.Amount).HasColumnType("decimal(8, 5)");

                entity.Property(e => e.GmWgt)
                    .HasColumnName("Gm_Wgt")
                    .HasColumnType("decimal(7, 3)");

                entity.Property(e => e.MsreDesc)
                    .IsRequired()
                    .HasColumnName("Msre_Desc")
                    .HasMaxLength(84)
                    .IsUnicode(false);

                entity.Property(e => e.NumDataPts)
                    .HasColumnName("Num_Data_Pts")
                    .HasColumnType("decimal(6, 0)");

                entity.Property(e => e.StdDev)
                    .HasColumnName("Std_Dev")
                    .HasColumnType("decimal(7, 3)");

                entity.HasOne(d => d.NdbNoNavigation)
                    .WithMany(p => p.Weight)
                    .HasForeignKey(d => d.NdbNo)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK__WEIGHT__NDB_No__681373AD");
            });
        }
    }
}
