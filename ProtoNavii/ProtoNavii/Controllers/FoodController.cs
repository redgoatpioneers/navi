﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProtoNavii.DAL.Interfaces;
using ProtoNavii.Models;
using ProtoNavii.ViewModels;
using SQLitePCL;

namespace ProtoNavii.Controllers
{
    public class FoodController : Controller
    {
        private readonly IFoodRepository _foodRepository;
        private readonly INutrientTypeRepository _nutrientTypeRepository;

        public FoodController(IFoodRepository foodRepository, INutrientTypeRepository nutrientTypeRepository)
        {
            _foodRepository = foodRepository;
            _nutrientTypeRepository = nutrientTypeRepository;
        }

        public IActionResult Index(string sortOrder)
        {
            var food = _foodRepository.GetAllFood().OrderBy(f => f.Name);
            var nutrientTypes = _nutrientTypeRepository.GetAllNutrientTypes()
                .OrderBy(nt => nt.Category)
                .ThenBy(nt => nt.Name);

            var foodViewModel = new FoodViewModel()
            {
                Title = "Food",
                Foods = food.ToList(),
                NutrientTypes = nutrientTypes.ToList()
            };

            return View(foodViewModel);
        }

        public IActionResult Details(int id)
        {
            var food = _foodRepository.GetFoodById(id);
            var nutrientTypes = _nutrientTypeRepository.GetAllNutrientTypes()
                .OrderBy(nt => nt.Category)
                .ThenBy(nt => nt.Name);

            if (food == null)
                return NotFound();

            var foodViewModel = new FoodViewModel()
            {
                Title = food.Name,
                Food = food,
                NutrientTypes = nutrientTypes.ToList()
            };

            return View(foodViewModel);
        }

        public IActionResult New()
        {
            var nutrientTypes = _nutrientTypeRepository.GetAllNutrientTypes()
                .OrderBy(nt => nt.Category)
                .ThenBy(nt => nt.Name);

            var foodViewModel = new FoodViewModel()
            {
                Title = "Add New Food",
                NutrientTypes = nutrientTypes.OrderBy(nt => nt.Category).ToList()
            };

            return View(foodViewModel);
        }

        public IActionResult Update(int id)
        {
            var nutrientTypes = _nutrientTypeRepository.GetAllNutrientTypes()
                .OrderBy(nt => nt.Category)
                .ThenBy(nt => nt.Name);

            var foodViewModel = new FoodViewModel()
            {
                Title = "Update Food",
                Food = _foodRepository.GetFoodById(id),
                NutrientTypes = nutrientTypes.OrderBy(nt => nt.Category).ToList()
            };

            return View(foodViewModel);
        }

        [HttpPost]
        public IActionResult CreateFood(Food food)
        {
            if (!ModelState.IsValid)
            {
                var nutrientTypes = _nutrientTypeRepository.GetAllNutrientTypes()
                    .OrderBy(nt => nt.Category)
                    .ThenBy(nt => nt.Name);
                var foodViewModel = new FoodViewModel()
                {
                    Title = "Food",
                    Food = food,
                    NutrientTypes = nutrientTypes.ToList()
                };

                return View("New", foodViewModel);
            }

            _foodRepository.CreateFood(food);
            return RedirectToAction("Index");

        }

        [HttpPut]
        public IActionResult UpdateFood(Food food)
        {
            if (!ModelState.IsValid)
            {
                var nutrientTypes = _nutrientTypeRepository.GetAllNutrientTypes()
                    .OrderBy(nt => nt.Category)
                    .ThenBy(nt => nt.Name);
                var foodViewModel = new FoodViewModel()
                {
                    Title = "Food",
                    Food = food,
                    NutrientTypes = nutrientTypes.ToList()
                };

                return View("Update", foodViewModel);
            }

            _foodRepository.UpdateFood(food);
            return RedirectToAction("Index");

        }

        [HttpDelete]
        public IActionResult DeleteFood(Food food)
        {
            _foodRepository.DeleteFood(food);
            return RedirectToAction("Index");

        }
    }
}