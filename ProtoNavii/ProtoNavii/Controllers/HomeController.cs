﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

using ProtoNavii.Models;
using ProtoNavii.DAL.Interfaces;
using ProtoNavii.ViewModels;

namespace ProtoNavii.Controllers
{
    public class HomeController : Controller
    {
        // GET: /<controller>/
        public IActionResult Index()
        {
            var homeViewModel = new HomeViewModel
            {
                Title = "ProtoNavii Home"
            };

            return View(homeViewModel);
        }
    }
}