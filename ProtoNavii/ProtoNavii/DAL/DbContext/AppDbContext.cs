﻿using Microsoft.EntityFrameworkCore;
using ProtoNavii.Models;

namespace ProtoNavii.DAL.DbContext
{
    public class AppDbContext: Microsoft.EntityFrameworkCore.DbContext
    {
        public AppDbContext(DbContextOptions options):base(options)
        {
            
        }

        public DbSet<Food> Foods { get; set; }
        public DbSet<FoodNutrient> Nutrients { get; set; }
        public DbSet<NutrientType> NutrientTypes { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Food>()
                .HasMany(f => f.FoodNutrients)
                .WithOne(fn => fn.Food);

            modelBuilder.Entity<Food>()
                .Property(f => f.Price)
                .HasColumnType("Money");

           modelBuilder.Entity<FoodNutrient>()
               .HasOne(f => f.NutrientType);

            modelBuilder.Entity<Recipe>()
                .HasMany(r => r.RecipeFoods)
                .WithOne(rf => rf.Recipe);

            modelBuilder.Entity<RecipeFood>()
                .HasOne(rf => rf.Food);
        }
    }
}
