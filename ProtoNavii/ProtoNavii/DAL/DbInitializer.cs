﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.DAL.DbContext;
using ProtoNavii.Enum;
using ProtoNavii.Models;

namespace ProtoNavii.DAL
{
    public static class DbInitializer
    {
        public static void Seed(AppDbContext context)
        {
            if (!context.Foods.Any())
            {
                context.AddRange
                (
                    new Food { Name = "Apple", Description = "Golden delicious apple.", ImageThumbnailUrl = "images/apple.jpg" },
                    new Food { Name = "Brown Rice", Description = "Rice, but brown!", ImageThumbnailUrl = "images/brownrice.jpg" },
                    new Food { Name = "Salmon", Description = "Fresh wild Alaskan salmon.", ImageThumbnailUrl = "images/salmon.jpg" },
                    new Food { Name = "Avocado", Description = "The best food ever.", ImageThumbnailUrl = "images/avocado.jpg" }
                );

                context.SaveChanges();
            }

            if (!context.NutrientTypes.Any())
            {
                context.AddRange
                (
                    new NutrientType { Name = "Calories", Description = "", Unit = NutrientUnit.kcal.ToString() },
                    new NutrientType { Name = "Protein", Description = "", Unit = NutrientUnit.g.ToString() },
                    new NutrientType { Name = "Fat", Description = "", Unit = NutrientUnit.g.ToString() },
                    new NutrientType { Name = "Carbohydrates", Description = "", Unit = NutrientUnit.g.ToString() },
                    new NutrientType { Name = "Fiber", Description = "", Unit = NutrientUnit.g.ToString() },
                    new NutrientType { Name = "Sugars", Description = "", Unit = NutrientUnit.g.ToString() }
                );

                context.SaveChanges();
            }
        }
    }
}
