﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.Models;

namespace ProtoNavii.DAL.Interfaces
{
    public interface IFoodNutrientRepository
    {
        IEnumerable<FoodNutrient> GetAllNutrients();
        FoodNutrient GetNutrientById(int foodNutrientId);
        void CreateNutrient(FoodNutrient foodNutrient);
        void DeleteNutrient(FoodNutrient foodNutrient);
    }
}
