﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.Models;

namespace ProtoNavii.DAL.Interfaces
{
    public interface IFoodRepository
    {
        IEnumerable<Food> GetAllFood();
        Food GetFoodById(int foodId);
        void CreateFood(Food food);
        void UpdateFood(Food food);
        void DeleteFood(Food food);
    }
}
