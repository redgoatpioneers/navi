﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.Models;

namespace ProtoNavii.DAL.Interfaces
{
    public interface INutrientTypeRepository
    {
        IEnumerable<NutrientType> GetAllNutrientTypes();
        NutrientType GetNutrientTypeById(int nutrientTypeId);
        void CreateNutrientType(NutrientType nutrientType);
        void DeleteNutrientType(NutrientType nutrientType);
    }
}
