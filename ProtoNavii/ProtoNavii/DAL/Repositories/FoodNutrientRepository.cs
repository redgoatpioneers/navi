﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.DAL.DbContext;
using ProtoNavii.DAL.Interfaces;
using ProtoNavii.Models;

namespace ProtoNavii.DAL.Repositories
{
    public class FoodNutrientRepository: IFoodNutrientRepository
    {
        private readonly AppDbContext _appDbContext;

        public FoodNutrientRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<FoodNutrient> GetAllNutrients()
        {
            return _appDbContext.Nutrients;
        }

        public FoodNutrient GetNutrientById(int foodNutrientId)
        {
            return _appDbContext.Nutrients.FirstOrDefault(f => f.FoodNutrientId == foodNutrientId);
        }

        public void CreateNutrient(FoodNutrient foodNutrient)
        {
            _appDbContext.Nutrients.Add(foodNutrient);
            _appDbContext.SaveChanges();
        }

        public void DeleteNutrient(FoodNutrient foodNutrient)
        {
            _appDbContext.Nutrients.Remove(foodNutrient);
            _appDbContext.SaveChanges();
        }
    }
}
