﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using ProtoNavii.DAL.DbContext;
using ProtoNavii.DAL.Interfaces;
using ProtoNavii.Models;

namespace ProtoNavii.DAL.Repositories
{
    public class FoodRepository: IFoodRepository
    {
        private readonly AppDbContext _appDbContext;

        public FoodRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<Food> GetAllFood()
        {
            return _appDbContext.Foods.Include(f => f.FoodNutrients).ToList();
        }

        public Food GetFoodById(int foodId)
        {
            return _appDbContext.Foods.Include(f => f.FoodNutrients).FirstOrDefault(f => f.FoodId == foodId);
        }

        public void CreateFood(Food food)
        {
            _appDbContext.Foods.Add(food);
            _appDbContext.SaveChanges();
        }

        public void UpdateFood(Food food)
        {
            _appDbContext.Foods.Update(food);
            _appDbContext.SaveChanges();
        }

        public void DeleteFood(Food food)
        {
            _appDbContext.Foods.Remove(food);
            _appDbContext.SaveChanges();
        }
    }
}
