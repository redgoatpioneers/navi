﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.DAL.DbContext;
using ProtoNavii.DAL.Interfaces;
using ProtoNavii.Models;

namespace ProtoNavii.DAL.Repositories
{
    public class NutrientTypeRepository: INutrientTypeRepository
    {
        private readonly AppDbContext _appDbContext;

        public NutrientTypeRepository(AppDbContext appDbContext)
        {
            _appDbContext = appDbContext;
        }

        public IEnumerable<NutrientType> GetAllNutrientTypes()
        {
            return _appDbContext.NutrientTypes;
        }

        public NutrientType GetNutrientTypeById(int NutrientTypeId)
        {
            return _appDbContext.NutrientTypes.FirstOrDefault(f => f.NutrientTypeId == NutrientTypeId);
        }

        public void CreateNutrientType(NutrientType NutrientType)
        {
            _appDbContext.NutrientTypes.Add(NutrientType);
            _appDbContext.SaveChanges();
        }

        public void DeleteNutrientType(NutrientType NutrientType)
        {
            _appDbContext.NutrientTypes.Remove(NutrientType);
            _appDbContext.SaveChanges();
        }
    }
}
