﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.DAL.DbContext;
using ProtoNavii.DAL.Interfaces;
using ProtoNavii.Models;

namespace ProtoNavii.Enum
{
    public enum NutrientUnit
    {
        ug,
        mg,
        g,
        kg,
        cal,
        kcal,
        TC
    }
}
