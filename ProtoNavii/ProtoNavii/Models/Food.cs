﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ProtoNavii.Models
{
    [Table("Foods")]
    public class Food
    {
        [BindNever]
        public int FoodId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(300)]
        public string Description { get; set; }
        [Column(TypeName="Money")]
        public decimal Price { get; set; }
        [StringLength(100)]
        public string ImageThumbnailUrl { get; set; }
        [Required]
        public List<FoodNutrient> FoodNutrients { get; set; }
        public bool IsActive { get; set; }
    }
}
