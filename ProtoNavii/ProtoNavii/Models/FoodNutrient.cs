﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ProtoNavii.Models
{
    [Table("FoodNutrients")]
    public class FoodNutrient
    {
        [BindNever]
        public int FoodNutrientId { get; set; }
        public int NutrientTypeId { get; set; }
        public NutrientType NutrientType { get; set; }
        public int FoodId { get; set; }
        public Food Food { get; set; }
        public decimal Amount { get; set; }
    }
}
