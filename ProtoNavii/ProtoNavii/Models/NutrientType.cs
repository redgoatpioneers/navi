﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.Enum;

namespace ProtoNavii.Models
{
    [Table("NutrientTypes")]
    public class NutrientType
    {
        public int NutrientTypeId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public NutrientCategory Category { get; set; }
        public string Unit { get; set; }
    }
}
