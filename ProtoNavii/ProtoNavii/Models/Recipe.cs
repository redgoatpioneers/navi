﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ProtoNavii.Models
{
    [Table("Recipies")]
    public class Recipe
    {
        [BindNever]
        public int RecipeId { get; set; }
        [Required]
        [StringLength(50)]
        public string Name { get; set; }
        [Required]
        [StringLength(300)]
        public string Description { get; set; }
        public string Type { get; set; }
        public string Category { get; set; }
        public string Instructions { get; set; }
        [StringLength(300)]
        public string Source { get; set; }
        [StringLength(100)]
        public string ImageThumbnailUrl { get; set; }
        [Required]
        public List<RecipeFood> RecipeFoods { get; set; }
        public bool IsActive { get; set; }
    }
}
