﻿using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Mvc.ModelBinding;

namespace ProtoNavii.Models
{
    [Table("RecipeFoods")]
    public class RecipeFood
    {
        [BindNever]
        public int RecipeFoodId { get; set; }
        public int RecipeId { get; set; }
        public Recipe Recipe { get; set; }
        public int FoodId { get; set; }
        public Food Food { get; set; }
        public decimal Amount { get; set; }
        public decimal RecipeQuantity { get; set; }
        public string RecipeUnit { get; set; }
    }
}
