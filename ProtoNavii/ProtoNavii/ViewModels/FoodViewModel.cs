﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.Models;

namespace ProtoNavii.ViewModels
{
    public class FoodViewModel
    {
        public string Title { get; set; }
        public List<Food> Foods { get; set; }
        public Food Food { get; set; }
        public List<NutrientType> NutrientTypes { get; set; }
    }
}
