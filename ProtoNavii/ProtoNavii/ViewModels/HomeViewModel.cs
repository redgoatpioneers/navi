﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.Models;

namespace ProtoNavii.ViewModels
{
    public class HomeViewModel
    {
        public string Title { get; set; }
    }
}
