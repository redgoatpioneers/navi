﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ProtoNavii.Models;

namespace ProtoNavii.ViewModels
{
    public class NutrientTypeViewModel
    {
        public string Title { get; set; }
        public List<NutrientType> NutrientTypes { get; set; }
    }
}
