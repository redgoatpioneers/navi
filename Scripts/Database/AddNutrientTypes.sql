USE ProtoNaviDb;
--DELETE FROM NutrientTypes;
SELECT * FROM NutrientTypes;

INSERT INTO NutrientTypes
(Name, Unit)
VALUES
('Calories','kcal'),
('Protein','g'),
('Fat','g'),
('Carbohydrate','g'),
('Fiber','g'),
('Sugars','g'),
('Calcium','mg'),
('Iron','mg'),
('Magnesium','mg'),
('Phosphorus','mg'),
('Potassium','mg'),
('Sodium','mg'),
('Zinc','mg'),
('Vitamin C','mg'),
('Thiamin','mg'),
('Riboflavin','mg'),
('Niacin','mg'),
('Vitamin B-6','mg'),
('Folate','ug'),
('Vitamin B-12','ug'),
('Vitamin A','ug'),
('Vitamin E','mg'),
('Vitamin D','ug'),
('Vitamin K','ug'),
('Saturated Fat','g'),
('Monounsaturated Fat','g'),
('Polyunsaturated Fat','g'),
('Trans Fat','g'),
('Cholesterol','TC')