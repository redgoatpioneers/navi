SELECT DISTINCT fd.FdGrp_Desc Category, f.NDB_No USDAFoodID, f.Long_Desc, f.Shrt_Desc,
    nf203.NutrDesc Nutr203Desc, CONCAT(nd203.Nutr_Val, ' ', nf203.Units) Nutr203Amount, 
    nf204.NutrDesc Nutr204Desc, CONCAT(nd204.Nutr_Val, ' ', nf204.Units) Nutr204Amount, 
    nf205.NutrDesc Nutr205Desc, CONCAT(nd205.Nutr_Val, ' ', nf205.Units) Nutr205Amount, 
    nf207.NutrDesc Nutr207Desc, CONCAT(nd207.Nutr_Val, ' ', nf207.Units) Nutr207Amount, 
    nf208.NutrDesc Nutr208Desc, CONCAT(nd208.Nutr_Val, ' ', nf208.Units) Nutr208Amount, 
    nf209.NutrDesc Nutr209Desc, CONCAT(nd209.Nutr_Val, ' ', nf209.Units) Nutr209Amount, 
    nf210.NutrDesc Nutr210Desc, CONCAT(nd210.Nutr_Val, ' ', nf210.Units) Nutr210Amount, 
    nf211.NutrDesc Nutr211Desc, CONCAT(nd211.Nutr_Val, ' ', nf211.Units) Nutr211Amount, 
    nf212.NutrDesc Nutr212Desc, CONCAT(nd212.Nutr_Val, ' ', nf212.Units) Nutr212Amount, 
    nf213.NutrDesc Nutr213Desc, CONCAT(nd213.Nutr_Val, ' ', nf213.Units) Nutr213Amount, 
    nf214.NutrDesc Nutr214Desc, CONCAT(nd214.Nutr_Val, ' ', nf214.Units) Nutr214Amount, 
    nf221.NutrDesc Nutr221Desc, CONCAT(nd221.Nutr_Val, ' ', nf221.Units) Nutr221Amount, 
    nf255.NutrDesc Nutr255Desc, CONCAT(nd255.Nutr_Val, ' ', nf255.Units) Nutr255Amount, 
    nf262.NutrDesc Nutr262Desc, CONCAT(nd262.Nutr_Val, ' ', nf262.Units) Nutr262Amount, 
    nf263.NutrDesc Nutr263Desc, CONCAT(nd263.Nutr_Val, ' ', nf263.Units) Nutr263Amount, 
    nf268.NutrDesc Nutr268Desc, CONCAT(nd268.Nutr_Val, ' ', nf268.Units) Nutr268Amount, 
    nf269.NutrDesc Nutr269Desc, CONCAT(nd269.Nutr_Val, ' ', nf269.Units) Nutr269Amount, 
    nf287.NutrDesc Nutr287Desc, CONCAT(nd287.Nutr_Val, ' ', nf287.Units) Nutr287Amount, 
    nf291.NutrDesc Nutr291Desc, CONCAT(nd291.Nutr_Val, ' ', nf291.Units) Nutr291Amount, 
    nf301.NutrDesc Nutr301Desc, CONCAT(nd301.Nutr_Val, ' ', nf301.Units) Nutr301Amount, 
    nf303.NutrDesc Nutr303Desc, CONCAT(nd303.Nutr_Val, ' ', nf303.Units) Nutr303Amount, 
    nf304.NutrDesc Nutr304Desc, CONCAT(nd304.Nutr_Val, ' ', nf304.Units) Nutr304Amount, 
    nf305.NutrDesc Nutr305Desc, CONCAT(nd305.Nutr_Val, ' ', nf305.Units) Nutr305Amount, 
    nf306.NutrDesc Nutr306Desc, CONCAT(nd306.Nutr_Val, ' ', nf306.Units) Nutr306Amount, 
    nf307.NutrDesc Nutr307Desc, CONCAT(nd307.Nutr_Val, ' ', nf307.Units) Nutr307Amount, 
    nf309.NutrDesc Nutr309Desc, CONCAT(nd309.Nutr_Val, ' ', nf309.Units) Nutr309Amount, 
    nf312.NutrDesc Nutr312Desc, CONCAT(nd312.Nutr_Val, ' ', nf312.Units) Nutr312Amount, 
    nf313.NutrDesc Nutr313Desc, CONCAT(nd313.Nutr_Val, ' ', nf313.Units) Nutr313Amount, 
    nf315.NutrDesc Nutr315Desc, CONCAT(nd315.Nutr_Val, ' ', nf315.Units) Nutr315Amount, 
    nf317.NutrDesc Nutr317Desc, CONCAT(nd317.Nutr_Val, ' ', nf317.Units) Nutr317Amount, 
    nf318.NutrDesc Nutr318Desc, CONCAT(nd318.Nutr_Val, ' ', nf318.Units) Nutr318Amount, 
    nf319.NutrDesc Nutr319Desc, CONCAT(nd319.Nutr_Val, ' ', nf319.Units) Nutr319Amount, 
    nf320.NutrDesc Nutr320Desc, CONCAT(nd320.Nutr_Val, ' ', nf320.Units) Nutr320Amount, 
    nf321.NutrDesc Nutr321Desc, CONCAT(nd321.Nutr_Val, ' ', nf321.Units) Nutr321Amount, 
    nf322.NutrDesc Nutr322Desc, CONCAT(nd322.Nutr_Val, ' ', nf322.Units) Nutr322Amount, 
    nf323.NutrDesc Nutr323Desc, CONCAT(nd323.Nutr_Val, ' ', nf323.Units) Nutr323Amount, 
    nf324.NutrDesc Nutr324Desc, CONCAT(nd324.Nutr_Val, ' ', nf324.Units) Nutr324Amount, 
    nf325.NutrDesc Nutr325Desc, CONCAT(nd325.Nutr_Val, ' ', nf325.Units) Nutr325Amount, 
    nf326.NutrDesc Nutr326Desc, CONCAT(nd326.Nutr_Val, ' ', nf326.Units) Nutr326Amount, 
    nf328.NutrDesc Nutr328Desc, CONCAT(nd328.Nutr_Val, ' ', nf328.Units) Nutr328Amount, 
    nf334.NutrDesc Nutr334Desc, CONCAT(nd334.Nutr_Val, ' ', nf334.Units) Nutr334Amount, 
    nf337.NutrDesc Nutr337Desc, CONCAT(nd337.Nutr_Val, ' ', nf337.Units) Nutr337Amount, 
    nf338.NutrDesc Nutr338Desc, CONCAT(nd338.Nutr_Val, ' ', nf338.Units) Nutr338Amount, 
    nf341.NutrDesc Nutr341Desc, CONCAT(nd341.Nutr_Val, ' ', nf341.Units) Nutr341Amount, 
    nf342.NutrDesc Nutr342Desc, CONCAT(nd342.Nutr_Val, ' ', nf342.Units) Nutr342Amount, 
    nf343.NutrDesc Nutr343Desc, CONCAT(nd343.Nutr_Val, ' ', nf343.Units) Nutr343Amount, 
    nf344.NutrDesc Nutr344Desc, CONCAT(nd344.Nutr_Val, ' ', nf344.Units) Nutr344Amount, 
    nf345.NutrDesc Nutr345Desc, CONCAT(nd345.Nutr_Val, ' ', nf345.Units) Nutr345Amount, 
    nf346.NutrDesc Nutr346Desc, CONCAT(nd346.Nutr_Val, ' ', nf346.Units) Nutr346Amount, 
    nf347.NutrDesc Nutr347Desc, CONCAT(nd347.Nutr_Val, ' ', nf347.Units) Nutr347Amount, 
    nf401.NutrDesc Nutr401Desc, CONCAT(nd401.Nutr_Val, ' ', nf401.Units) Nutr401Amount, 
    nf404.NutrDesc Nutr404Desc, CONCAT(nd404.Nutr_Val, ' ', nf404.Units) Nutr404Amount, 
    nf405.NutrDesc Nutr405Desc, CONCAT(nd405.Nutr_Val, ' ', nf405.Units) Nutr405Amount, 
    nf406.NutrDesc Nutr406Desc, CONCAT(nd406.Nutr_Val, ' ', nf406.Units) Nutr406Amount, 
    nf410.NutrDesc Nutr410Desc, CONCAT(nd410.Nutr_Val, ' ', nf410.Units) Nutr410Amount, 
    nf415.NutrDesc Nutr415Desc, CONCAT(nd415.Nutr_Val, ' ', nf415.Units) Nutr415Amount, 
    nf417.NutrDesc Nutr417Desc, CONCAT(nd417.Nutr_Val, ' ', nf417.Units) Nutr417Amount, 
    nf418.NutrDesc Nutr418Desc, CONCAT(nd418.Nutr_Val, ' ', nf418.Units) Nutr418Amount, 
    nf421.NutrDesc Nutr421Desc, CONCAT(nd421.Nutr_Val, ' ', nf421.Units) Nutr421Amount, 
    nf428.NutrDesc Nutr428Desc, CONCAT(nd428.Nutr_Val, ' ', nf428.Units) Nutr428Amount, 
    nf429.NutrDesc Nutr429Desc, CONCAT(nd429.Nutr_Val, ' ', nf429.Units) Nutr429Amount, 
    nf430.NutrDesc Nutr430Desc, CONCAT(nd430.Nutr_Val, ' ', nf430.Units) Nutr430Amount, 
    nf431.NutrDesc Nutr431Desc, CONCAT(nd431.Nutr_Val, ' ', nf431.Units) Nutr431Amount, 
    nf432.NutrDesc Nutr432Desc, CONCAT(nd432.Nutr_Val, ' ', nf432.Units) Nutr432Amount, 
    nf435.NutrDesc Nutr435Desc, CONCAT(nd435.Nutr_Val, ' ', nf435.Units) Nutr435Amount, 
    nf454.NutrDesc Nutr454Desc, CONCAT(nd454.Nutr_Val, ' ', nf454.Units) Nutr454Amount, 
    nf501.NutrDesc Nutr501Desc, CONCAT(nd501.Nutr_Val, ' ', nf501.Units) Nutr501Amount, 
    nf502.NutrDesc Nutr502Desc, CONCAT(nd502.Nutr_Val, ' ', nf502.Units) Nutr502Amount, 
    nf503.NutrDesc Nutr503Desc, CONCAT(nd503.Nutr_Val, ' ', nf503.Units) Nutr503Amount, 
    nf504.NutrDesc Nutr504Desc, CONCAT(nd504.Nutr_Val, ' ', nf504.Units) Nutr504Amount, 
    nf505.NutrDesc Nutr505Desc, CONCAT(nd505.Nutr_Val, ' ', nf505.Units) Nutr505Amount, 
    nf506.NutrDesc Nutr506Desc, CONCAT(nd506.Nutr_Val, ' ', nf506.Units) Nutr506Amount, 
    nf507.NutrDesc Nutr507Desc, CONCAT(nd507.Nutr_Val, ' ', nf507.Units) Nutr507Amount, 
    nf508.NutrDesc Nutr508Desc, CONCAT(nd508.Nutr_Val, ' ', nf508.Units) Nutr508Amount, 
    nf509.NutrDesc Nutr509Desc, CONCAT(nd509.Nutr_Val, ' ', nf509.Units) Nutr509Amount, 
    nf510.NutrDesc Nutr510Desc, CONCAT(nd510.Nutr_Val, ' ', nf510.Units) Nutr510Amount, 
    nf511.NutrDesc Nutr511Desc, CONCAT(nd511.Nutr_Val, ' ', nf511.Units) Nutr511Amount, 
    nf512.NutrDesc Nutr512Desc, CONCAT(nd512.Nutr_Val, ' ', nf512.Units) Nutr512Amount, 
    nf513.NutrDesc Nutr513Desc, CONCAT(nd513.Nutr_Val, ' ', nf513.Units) Nutr513Amount, 
    nf514.NutrDesc Nutr514Desc, CONCAT(nd514.Nutr_Val, ' ', nf514.Units) Nutr514Amount, 
    nf515.NutrDesc Nutr515Desc, CONCAT(nd515.Nutr_Val, ' ', nf515.Units) Nutr515Amount, 
    nf516.NutrDesc Nutr516Desc, CONCAT(nd516.Nutr_Val, ' ', nf516.Units) Nutr516Amount, 
    nf517.NutrDesc Nutr517Desc, CONCAT(nd517.Nutr_Val, ' ', nf517.Units) Nutr517Amount, 
    nf518.NutrDesc Nutr518Desc, CONCAT(nd518.Nutr_Val, ' ', nf518.Units) Nutr518Amount, 
    nf521.NutrDesc Nutr521Desc, CONCAT(nd521.Nutr_Val, ' ', nf521.Units) Nutr521Amount, 
    nf573.NutrDesc Nutr573Desc, CONCAT(nd573.Nutr_Val, ' ', nf573.Units) Nutr573Amount, 
    nf578.NutrDesc Nutr578Desc, CONCAT(nd578.Nutr_Val, ' ', nf578.Units) Nutr578Amount, 
    nf601.NutrDesc Nutr601Desc, CONCAT(nd601.Nutr_Val, ' ', nf601.Units) Nutr601Amount, 
    nf605.NutrDesc Nutr605Desc, CONCAT(nd605.Nutr_Val, ' ', nf605.Units) Nutr605Amount, 
    nf606.NutrDesc Nutr606Desc, CONCAT(nd606.Nutr_Val, ' ', nf606.Units) Nutr606Amount, 
    nf607.NutrDesc Nutr607Desc, CONCAT(nd607.Nutr_Val, ' ', nf607.Units) Nutr607Amount, 
    nf608.NutrDesc Nutr608Desc, CONCAT(nd608.Nutr_Val, ' ', nf608.Units) Nutr608Amount, 
    nf609.NutrDesc Nutr609Desc, CONCAT(nd609.Nutr_Val, ' ', nf609.Units) Nutr609Amount, 
    nf610.NutrDesc Nutr610Desc, CONCAT(nd610.Nutr_Val, ' ', nf610.Units) Nutr610Amount, 
    nf611.NutrDesc Nutr611Desc, CONCAT(nd611.Nutr_Val, ' ', nf611.Units) Nutr611Amount, 
    nf612.NutrDesc Nutr612Desc, CONCAT(nd612.Nutr_Val, ' ', nf612.Units) Nutr612Amount, 
    nf613.NutrDesc Nutr613Desc, CONCAT(nd613.Nutr_Val, ' ', nf613.Units) Nutr613Amount, 
    nf614.NutrDesc Nutr614Desc, CONCAT(nd614.Nutr_Val, ' ', nf614.Units) Nutr614Amount, 
    nf615.NutrDesc Nutr615Desc, CONCAT(nd615.Nutr_Val, ' ', nf615.Units) Nutr615Amount, 
    nf617.NutrDesc Nutr617Desc, CONCAT(nd617.Nutr_Val, ' ', nf617.Units) Nutr617Amount, 
    nf618.NutrDesc Nutr618Desc, CONCAT(nd618.Nutr_Val, ' ', nf618.Units) Nutr618Amount, 
    nf619.NutrDesc Nutr619Desc, CONCAT(nd619.Nutr_Val, ' ', nf619.Units) Nutr619Amount, 
    nf620.NutrDesc Nutr620Desc, CONCAT(nd620.Nutr_Val, ' ', nf620.Units) Nutr620Amount, 
    nf621.NutrDesc Nutr621Desc, CONCAT(nd621.Nutr_Val, ' ', nf621.Units) Nutr621Amount, 
    nf624.NutrDesc Nutr624Desc, CONCAT(nd624.Nutr_Val, ' ', nf624.Units) Nutr624Amount, 
    nf625.NutrDesc Nutr625Desc, CONCAT(nd625.Nutr_Val, ' ', nf625.Units) Nutr625Amount, 
    nf626.NutrDesc Nutr626Desc, CONCAT(nd626.Nutr_Val, ' ', nf626.Units) Nutr626Amount, 
    nf627.NutrDesc Nutr627Desc, CONCAT(nd627.Nutr_Val, ' ', nf627.Units) Nutr627Amount, 
    nf628.NutrDesc Nutr628Desc, CONCAT(nd628.Nutr_Val, ' ', nf628.Units) Nutr628Amount, 
    nf629.NutrDesc Nutr629Desc, CONCAT(nd629.Nutr_Val, ' ', nf629.Units) Nutr629Amount, 
    nf630.NutrDesc Nutr630Desc, CONCAT(nd630.Nutr_Val, ' ', nf630.Units) Nutr630Amount, 
    nf631.NutrDesc Nutr631Desc, CONCAT(nd631.Nutr_Val, ' ', nf631.Units) Nutr631Amount, 
    nf636.NutrDesc Nutr636Desc, CONCAT(nd636.Nutr_Val, ' ', nf636.Units) Nutr636Amount, 
    nf638.NutrDesc Nutr638Desc, CONCAT(nd638.Nutr_Val, ' ', nf638.Units) Nutr638Amount, 
    nf639.NutrDesc Nutr639Desc, CONCAT(nd639.Nutr_Val, ' ', nf639.Units) Nutr639Amount, 
    nf641.NutrDesc Nutr641Desc, CONCAT(nd641.Nutr_Val, ' ', nf641.Units) Nutr641Amount, 
    nf645.NutrDesc Nutr645Desc, CONCAT(nd645.Nutr_Val, ' ', nf645.Units) Nutr645Amount, 
    nf646.NutrDesc Nutr646Desc, CONCAT(nd646.Nutr_Val, ' ', nf646.Units) Nutr646Amount, 
    nf652.NutrDesc Nutr652Desc, CONCAT(nd652.Nutr_Val, ' ', nf652.Units) Nutr652Amount, 
    nf653.NutrDesc Nutr653Desc, CONCAT(nd653.Nutr_Val, ' ', nf653.Units) Nutr653Amount, 
    nf654.NutrDesc Nutr654Desc, CONCAT(nd654.Nutr_Val, ' ', nf654.Units) Nutr654Amount, 
    nf662.NutrDesc Nutr662Desc, CONCAT(nd662.Nutr_Val, ' ', nf662.Units) Nutr662Amount, 
    nf663.NutrDesc Nutr663Desc, CONCAT(nd663.Nutr_Val, ' ', nf663.Units) Nutr663Amount, 
    nf664.NutrDesc Nutr664Desc, CONCAT(nd664.Nutr_Val, ' ', nf664.Units) Nutr664Amount, 
    nf665.NutrDesc Nutr665Desc, CONCAT(nd665.Nutr_Val, ' ', nf665.Units) Nutr665Amount, 
    nf666.NutrDesc Nutr666Desc, CONCAT(nd666.Nutr_Val, ' ', nf666.Units) Nutr666Amount, 
    nf669.NutrDesc Nutr669Desc, CONCAT(nd669.Nutr_Val, ' ', nf669.Units) Nutr669Amount, 
    nf670.NutrDesc Nutr670Desc, CONCAT(nd670.Nutr_Val, ' ', nf670.Units) Nutr670Amount, 
    nf671.NutrDesc Nutr671Desc, CONCAT(nd671.Nutr_Val, ' ', nf671.Units) Nutr671Amount, 
    nf672.NutrDesc Nutr672Desc, CONCAT(nd672.Nutr_Val, ' ', nf672.Units) Nutr672Amount, 
    nf673.NutrDesc Nutr673Desc, CONCAT(nd673.Nutr_Val, ' ', nf673.Units) Nutr673Amount, 
    nf674.NutrDesc Nutr674Desc, CONCAT(nd674.Nutr_Val, ' ', nf674.Units) Nutr674Amount, 
    nf675.NutrDesc Nutr675Desc, CONCAT(nd675.Nutr_Val, ' ', nf675.Units) Nutr675Amount, 
    nf676.NutrDesc Nutr676Desc, CONCAT(nd676.Nutr_Val, ' ', nf676.Units) Nutr676Amount, 
    nf685.NutrDesc Nutr685Desc, CONCAT(nd685.Nutr_Val, ' ', nf685.Units) Nutr685Amount, 
    nf687.NutrDesc Nutr687Desc, CONCAT(nd687.Nutr_Val, ' ', nf687.Units) Nutr687Amount, 
    nf689.NutrDesc Nutr689Desc, CONCAT(nd689.Nutr_Val, ' ', nf689.Units) Nutr689Amount, 
    nf693.NutrDesc Nutr693Desc, CONCAT(nd693.Nutr_Val, ' ', nf693.Units) Nutr693Amount, 
    nf695.NutrDesc Nutr695Desc, CONCAT(nd695.Nutr_Val, ' ', nf695.Units) Nutr695Amount, 
    nf696.NutrDesc Nutr696Desc, CONCAT(nd696.Nutr_Val, ' ', nf696.Units) Nutr696Amount, 
    nf697.NutrDesc Nutr697Desc, CONCAT(nd697.Nutr_Val, ' ', nf697.Units) Nutr697Amount, 
    nf851.NutrDesc Nutr851Desc, CONCAT(nd851.Nutr_Val, ' ', nf851.Units) Nutr851Amount, 
    nf852.NutrDesc Nutr852Desc, CONCAT(nd852.Nutr_Val, ' ', nf852.Units) Nutr852Amount, 
    nf853.NutrDesc Nutr853Desc, CONCAT(nd853.Nutr_Val, ' ', nf853.Units) Nutr853Amount, 
    nf855.NutrDesc Nutr855Desc, CONCAT(nd855.Nutr_Val, ' ', nf855.Units) Nutr855Amount, 
    nf856.NutrDesc Nutr856Desc, CONCAT(nd856.Nutr_Val, ' ', nf856.Units) Nutr856Amount, 
    nf857.NutrDesc Nutr857Desc, CONCAT(nd857.Nutr_Val, ' ', nf857.Units) Nutr857Amount, 
    nf858.NutrDesc Nutr858Desc, CONCAT(nd858.Nutr_Val, ' ', nf858.Units) Nutr858Amount, 
    nf859.NutrDesc Nutr859Desc, CONCAT(nd859.Nutr_Val, ' ', nf859.Units) Nutr859Amount
FROM [usda].FOOD_DES f INNER JOIN [usda].FD_GROUP fd ON f.FdGrp_Cd = fd.FdGrp_Cd
    INNER JOIN [usda].NUT_DATA nd203 ON nd203.NDB_No = f.NDB_No AND nd203.Nutr_No = 203
    INNER JOIN [usda].NUTR_DEF nf203 ON nf203.Nutr_No = 203
    INNER JOIN [usda].NUT_DATA nd204 ON nd204.NDB_No = f.NDB_No AND nd204.Nutr_No = 204
    INNER JOIN [usda].NUTR_DEF nf204 ON nf204.Nutr_No = 204
    INNER JOIN [usda].NUT_DATA nd205 ON nd205.NDB_No = f.NDB_No AND nd205.Nutr_No = 205
    INNER JOIN [usda].NUTR_DEF nf205 ON nf205.Nutr_No = 205
    INNER JOIN [usda].NUT_DATA nd207 ON nd207.NDB_No = f.NDB_No AND nd207.Nutr_No = 207
    INNER JOIN [usda].NUTR_DEF nf207 ON nf207.Nutr_No = 207
    INNER JOIN [usda].NUT_DATA nd208 ON nd208.NDB_No = f.NDB_No AND nd208.Nutr_No = 208
    INNER JOIN [usda].NUTR_DEF nf208 ON nf208.Nutr_No = 208
    INNER JOIN [usda].NUT_DATA nd209 ON nd209.NDB_No = f.NDB_No AND nd209.Nutr_No = 209
    INNER JOIN [usda].NUTR_DEF nf209 ON nf209.Nutr_No = 209
    INNER JOIN [usda].NUT_DATA nd210 ON nd210.NDB_No = f.NDB_No AND nd210.Nutr_No = 210
    INNER JOIN [usda].NUTR_DEF nf210 ON nf210.Nutr_No = 210
    INNER JOIN [usda].NUT_DATA nd211 ON nd211.NDB_No = f.NDB_No AND nd211.Nutr_No = 211
    INNER JOIN [usda].NUTR_DEF nf211 ON nf211.Nutr_No = 211
    INNER JOIN [usda].NUT_DATA nd212 ON nd212.NDB_No = f.NDB_No AND nd212.Nutr_No = 212
    INNER JOIN [usda].NUTR_DEF nf212 ON nf212.Nutr_No = 212
    INNER JOIN [usda].NUT_DATA nd213 ON nd213.NDB_No = f.NDB_No AND nd213.Nutr_No = 213
    INNER JOIN [usda].NUTR_DEF nf213 ON nf213.Nutr_No = 213
    INNER JOIN [usda].NUT_DATA nd214 ON nd214.NDB_No = f.NDB_No AND nd214.Nutr_No = 214
    INNER JOIN [usda].NUTR_DEF nf214 ON nf214.Nutr_No = 214
    INNER JOIN [usda].NUT_DATA nd221 ON nd221.NDB_No = f.NDB_No AND nd221.Nutr_No = 221
    INNER JOIN [usda].NUTR_DEF nf221 ON nf221.Nutr_No = 221
    INNER JOIN [usda].NUT_DATA nd255 ON nd255.NDB_No = f.NDB_No AND nd255.Nutr_No = 255
    INNER JOIN [usda].NUTR_DEF nf255 ON nf255.Nutr_No = 255
    INNER JOIN [usda].NUT_DATA nd262 ON nd262.NDB_No = f.NDB_No AND nd262.Nutr_No = 262
    INNER JOIN [usda].NUTR_DEF nf262 ON nf262.Nutr_No = 262
    INNER JOIN [usda].NUT_DATA nd263 ON nd263.NDB_No = f.NDB_No AND nd263.Nutr_No = 263
    INNER JOIN [usda].NUTR_DEF nf263 ON nf263.Nutr_No = 263
    INNER JOIN [usda].NUT_DATA nd268 ON nd268.NDB_No = f.NDB_No AND nd268.Nutr_No = 268
    INNER JOIN [usda].NUTR_DEF nf268 ON nf268.Nutr_No = 268
    INNER JOIN [usda].NUT_DATA nd269 ON nd269.NDB_No = f.NDB_No AND nd269.Nutr_No = 269
    INNER JOIN [usda].NUTR_DEF nf269 ON nf269.Nutr_No = 269
    INNER JOIN [usda].NUT_DATA nd287 ON nd287.NDB_No = f.NDB_No AND nd287.Nutr_No = 287
    INNER JOIN [usda].NUTR_DEF nf287 ON nf287.Nutr_No = 287
    INNER JOIN [usda].NUT_DATA nd291 ON nd291.NDB_No = f.NDB_No AND nd291.Nutr_No = 291
    INNER JOIN [usda].NUTR_DEF nf291 ON nf291.Nutr_No = 291
    INNER JOIN [usda].NUT_DATA nd301 ON nd301.NDB_No = f.NDB_No AND nd301.Nutr_No = 301
    INNER JOIN [usda].NUTR_DEF nf301 ON nf301.Nutr_No = 301
    INNER JOIN [usda].NUT_DATA nd303 ON nd303.NDB_No = f.NDB_No AND nd303.Nutr_No = 303
    INNER JOIN [usda].NUTR_DEF nf303 ON nf303.Nutr_No = 303
    INNER JOIN [usda].NUT_DATA nd304 ON nd304.NDB_No = f.NDB_No AND nd304.Nutr_No = 304
    INNER JOIN [usda].NUTR_DEF nf304 ON nf304.Nutr_No = 304
    INNER JOIN [usda].NUT_DATA nd305 ON nd305.NDB_No = f.NDB_No AND nd305.Nutr_No = 305
    INNER JOIN [usda].NUTR_DEF nf305 ON nf305.Nutr_No = 305
    INNER JOIN [usda].NUT_DATA nd306 ON nd306.NDB_No = f.NDB_No AND nd306.Nutr_No = 306
    INNER JOIN [usda].NUTR_DEF nf306 ON nf306.Nutr_No = 306
    INNER JOIN [usda].NUT_DATA nd307 ON nd307.NDB_No = f.NDB_No AND nd307.Nutr_No = 307
    INNER JOIN [usda].NUTR_DEF nf307 ON nf307.Nutr_No = 307
    INNER JOIN [usda].NUT_DATA nd309 ON nd309.NDB_No = f.NDB_No AND nd309.Nutr_No = 309
    INNER JOIN [usda].NUTR_DEF nf309 ON nf309.Nutr_No = 309
    INNER JOIN [usda].NUT_DATA nd312 ON nd312.NDB_No = f.NDB_No AND nd312.Nutr_No = 312
    INNER JOIN [usda].NUTR_DEF nf312 ON nf312.Nutr_No = 312
    INNER JOIN [usda].NUT_DATA nd313 ON nd313.NDB_No = f.NDB_No AND nd313.Nutr_No = 313
    INNER JOIN [usda].NUTR_DEF nf313 ON nf313.Nutr_No = 313
    INNER JOIN [usda].NUT_DATA nd315 ON nd315.NDB_No = f.NDB_No AND nd315.Nutr_No = 315
    INNER JOIN [usda].NUTR_DEF nf315 ON nf315.Nutr_No = 315
    INNER JOIN [usda].NUT_DATA nd317 ON nd317.NDB_No = f.NDB_No AND nd317.Nutr_No = 317
    INNER JOIN [usda].NUTR_DEF nf317 ON nf317.Nutr_No = 317
    INNER JOIN [usda].NUT_DATA nd318 ON nd318.NDB_No = f.NDB_No AND nd318.Nutr_No = 318
    INNER JOIN [usda].NUTR_DEF nf318 ON nf318.Nutr_No = 318
    INNER JOIN [usda].NUT_DATA nd319 ON nd319.NDB_No = f.NDB_No AND nd319.Nutr_No = 319
    INNER JOIN [usda].NUTR_DEF nf319 ON nf319.Nutr_No = 319
    INNER JOIN [usda].NUT_DATA nd320 ON nd320.NDB_No = f.NDB_No AND nd320.Nutr_No = 320
    INNER JOIN [usda].NUTR_DEF nf320 ON nf320.Nutr_No = 320
    INNER JOIN [usda].NUT_DATA nd321 ON nd321.NDB_No = f.NDB_No AND nd321.Nutr_No = 321
    INNER JOIN [usda].NUTR_DEF nf321 ON nf321.Nutr_No = 321
    INNER JOIN [usda].NUT_DATA nd322 ON nd322.NDB_No = f.NDB_No AND nd322.Nutr_No = 322
    INNER JOIN [usda].NUTR_DEF nf322 ON nf322.Nutr_No = 322
    INNER JOIN [usda].NUT_DATA nd323 ON nd323.NDB_No = f.NDB_No AND nd323.Nutr_No = 323
    INNER JOIN [usda].NUTR_DEF nf323 ON nf323.Nutr_No = 323
    INNER JOIN [usda].NUT_DATA nd324 ON nd324.NDB_No = f.NDB_No AND nd324.Nutr_No = 324
    INNER JOIN [usda].NUTR_DEF nf324 ON nf324.Nutr_No = 324
    INNER JOIN [usda].NUT_DATA nd325 ON nd325.NDB_No = f.NDB_No AND nd325.Nutr_No = 325
    INNER JOIN [usda].NUTR_DEF nf325 ON nf325.Nutr_No = 325
    INNER JOIN [usda].NUT_DATA nd326 ON nd326.NDB_No = f.NDB_No AND nd326.Nutr_No = 326
    INNER JOIN [usda].NUTR_DEF nf326 ON nf326.Nutr_No = 326
    INNER JOIN [usda].NUT_DATA nd328 ON nd328.NDB_No = f.NDB_No AND nd328.Nutr_No = 328
    INNER JOIN [usda].NUTR_DEF nf328 ON nf328.Nutr_No = 328
    INNER JOIN [usda].NUT_DATA nd334 ON nd334.NDB_No = f.NDB_No AND nd334.Nutr_No = 334
    INNER JOIN [usda].NUTR_DEF nf334 ON nf334.Nutr_No = 334
    INNER JOIN [usda].NUT_DATA nd337 ON nd337.NDB_No = f.NDB_No AND nd337.Nutr_No = 337
    INNER JOIN [usda].NUTR_DEF nf337 ON nf337.Nutr_No = 337
    INNER JOIN [usda].NUT_DATA nd338 ON nd338.NDB_No = f.NDB_No AND nd338.Nutr_No = 338
    INNER JOIN [usda].NUTR_DEF nf338 ON nf338.Nutr_No = 338
    INNER JOIN [usda].NUT_DATA nd341 ON nd341.NDB_No = f.NDB_No AND nd341.Nutr_No = 341
    INNER JOIN [usda].NUTR_DEF nf341 ON nf341.Nutr_No = 341
    INNER JOIN [usda].NUT_DATA nd342 ON nd342.NDB_No = f.NDB_No AND nd342.Nutr_No = 342
    INNER JOIN [usda].NUTR_DEF nf342 ON nf342.Nutr_No = 342
    INNER JOIN [usda].NUT_DATA nd343 ON nd343.NDB_No = f.NDB_No AND nd343.Nutr_No = 343
    INNER JOIN [usda].NUTR_DEF nf343 ON nf343.Nutr_No = 343
    INNER JOIN [usda].NUT_DATA nd344 ON nd344.NDB_No = f.NDB_No AND nd344.Nutr_No = 344
    INNER JOIN [usda].NUTR_DEF nf344 ON nf344.Nutr_No = 344
    INNER JOIN [usda].NUT_DATA nd345 ON nd345.NDB_No = f.NDB_No AND nd345.Nutr_No = 345
    INNER JOIN [usda].NUTR_DEF nf345 ON nf345.Nutr_No = 345
    INNER JOIN [usda].NUT_DATA nd346 ON nd346.NDB_No = f.NDB_No AND nd346.Nutr_No = 346
    INNER JOIN [usda].NUTR_DEF nf346 ON nf346.Nutr_No = 346
    INNER JOIN [usda].NUT_DATA nd347 ON nd347.NDB_No = f.NDB_No AND nd347.Nutr_No = 347
    INNER JOIN [usda].NUTR_DEF nf347 ON nf347.Nutr_No = 347
    INNER JOIN [usda].NUT_DATA nd401 ON nd401.NDB_No = f.NDB_No AND nd401.Nutr_No = 401
    INNER JOIN [usda].NUTR_DEF nf401 ON nf401.Nutr_No = 401
    INNER JOIN [usda].NUT_DATA nd404 ON nd404.NDB_No = f.NDB_No AND nd404.Nutr_No = 404
    INNER JOIN [usda].NUTR_DEF nf404 ON nf404.Nutr_No = 404
    INNER JOIN [usda].NUT_DATA nd405 ON nd405.NDB_No = f.NDB_No AND nd405.Nutr_No = 405
    INNER JOIN [usda].NUTR_DEF nf405 ON nf405.Nutr_No = 405
    INNER JOIN [usda].NUT_DATA nd406 ON nd406.NDB_No = f.NDB_No AND nd406.Nutr_No = 406
    INNER JOIN [usda].NUTR_DEF nf406 ON nf406.Nutr_No = 406
    INNER JOIN [usda].NUT_DATA nd410 ON nd410.NDB_No = f.NDB_No AND nd410.Nutr_No = 410
    INNER JOIN [usda].NUTR_DEF nf410 ON nf410.Nutr_No = 410
    INNER JOIN [usda].NUT_DATA nd415 ON nd415.NDB_No = f.NDB_No AND nd415.Nutr_No = 415
    INNER JOIN [usda].NUTR_DEF nf415 ON nf415.Nutr_No = 415
    INNER JOIN [usda].NUT_DATA nd417 ON nd417.NDB_No = f.NDB_No AND nd417.Nutr_No = 417
    INNER JOIN [usda].NUTR_DEF nf417 ON nf417.Nutr_No = 417
    INNER JOIN [usda].NUT_DATA nd418 ON nd418.NDB_No = f.NDB_No AND nd418.Nutr_No = 418
    INNER JOIN [usda].NUTR_DEF nf418 ON nf418.Nutr_No = 418
    INNER JOIN [usda].NUT_DATA nd421 ON nd421.NDB_No = f.NDB_No AND nd421.Nutr_No = 421
    INNER JOIN [usda].NUTR_DEF nf421 ON nf421.Nutr_No = 421
    INNER JOIN [usda].NUT_DATA nd428 ON nd428.NDB_No = f.NDB_No AND nd428.Nutr_No = 428
    INNER JOIN [usda].NUTR_DEF nf428 ON nf428.Nutr_No = 428
    INNER JOIN [usda].NUT_DATA nd429 ON nd429.NDB_No = f.NDB_No AND nd429.Nutr_No = 429
    INNER JOIN [usda].NUTR_DEF nf429 ON nf429.Nutr_No = 429
    INNER JOIN [usda].NUT_DATA nd430 ON nd430.NDB_No = f.NDB_No AND nd430.Nutr_No = 430
    INNER JOIN [usda].NUTR_DEF nf430 ON nf430.Nutr_No = 430
    INNER JOIN [usda].NUT_DATA nd431 ON nd431.NDB_No = f.NDB_No AND nd431.Nutr_No = 431
    INNER JOIN [usda].NUTR_DEF nf431 ON nf431.Nutr_No = 431
    INNER JOIN [usda].NUT_DATA nd432 ON nd432.NDB_No = f.NDB_No AND nd432.Nutr_No = 432
    INNER JOIN [usda].NUTR_DEF nf432 ON nf432.Nutr_No = 432
    INNER JOIN [usda].NUT_DATA nd435 ON nd435.NDB_No = f.NDB_No AND nd435.Nutr_No = 435
    INNER JOIN [usda].NUTR_DEF nf435 ON nf435.Nutr_No = 435
    INNER JOIN [usda].NUT_DATA nd454 ON nd454.NDB_No = f.NDB_No AND nd454.Nutr_No = 454
    INNER JOIN [usda].NUTR_DEF nf454 ON nf454.Nutr_No = 454
    INNER JOIN [usda].NUT_DATA nd501 ON nd501.NDB_No = f.NDB_No AND nd501.Nutr_No = 501
    INNER JOIN [usda].NUTR_DEF nf501 ON nf501.Nutr_No = 501
    INNER JOIN [usda].NUT_DATA nd502 ON nd502.NDB_No = f.NDB_No AND nd502.Nutr_No = 502
    INNER JOIN [usda].NUTR_DEF nf502 ON nf502.Nutr_No = 502
    INNER JOIN [usda].NUT_DATA nd503 ON nd503.NDB_No = f.NDB_No AND nd503.Nutr_No = 503
    INNER JOIN [usda].NUTR_DEF nf503 ON nf503.Nutr_No = 503
    INNER JOIN [usda].NUT_DATA nd504 ON nd504.NDB_No = f.NDB_No AND nd504.Nutr_No = 504
    INNER JOIN [usda].NUTR_DEF nf504 ON nf504.Nutr_No = 504
    INNER JOIN [usda].NUT_DATA nd505 ON nd505.NDB_No = f.NDB_No AND nd505.Nutr_No = 505
    INNER JOIN [usda].NUTR_DEF nf505 ON nf505.Nutr_No = 505
    INNER JOIN [usda].NUT_DATA nd506 ON nd506.NDB_No = f.NDB_No AND nd506.Nutr_No = 506
    INNER JOIN [usda].NUTR_DEF nf506 ON nf506.Nutr_No = 506
    INNER JOIN [usda].NUT_DATA nd507 ON nd507.NDB_No = f.NDB_No AND nd507.Nutr_No = 507
    INNER JOIN [usda].NUTR_DEF nf507 ON nf507.Nutr_No = 507
    INNER JOIN [usda].NUT_DATA nd508 ON nd508.NDB_No = f.NDB_No AND nd508.Nutr_No = 508
    INNER JOIN [usda].NUTR_DEF nf508 ON nf508.Nutr_No = 508
    INNER JOIN [usda].NUT_DATA nd509 ON nd509.NDB_No = f.NDB_No AND nd509.Nutr_No = 509
    INNER JOIN [usda].NUTR_DEF nf509 ON nf509.Nutr_No = 509
    INNER JOIN [usda].NUT_DATA nd510 ON nd510.NDB_No = f.NDB_No AND nd510.Nutr_No = 510
    INNER JOIN [usda].NUTR_DEF nf510 ON nf510.Nutr_No = 510
    INNER JOIN [usda].NUT_DATA nd511 ON nd511.NDB_No = f.NDB_No AND nd511.Nutr_No = 511
    INNER JOIN [usda].NUTR_DEF nf511 ON nf511.Nutr_No = 511
    INNER JOIN [usda].NUT_DATA nd512 ON nd512.NDB_No = f.NDB_No AND nd512.Nutr_No = 512
    INNER JOIN [usda].NUTR_DEF nf512 ON nf512.Nutr_No = 512
    INNER JOIN [usda].NUT_DATA nd513 ON nd513.NDB_No = f.NDB_No AND nd513.Nutr_No = 513
    INNER JOIN [usda].NUTR_DEF nf513 ON nf513.Nutr_No = 513
    INNER JOIN [usda].NUT_DATA nd514 ON nd514.NDB_No = f.NDB_No AND nd514.Nutr_No = 514
    INNER JOIN [usda].NUTR_DEF nf514 ON nf514.Nutr_No = 514
    INNER JOIN [usda].NUT_DATA nd515 ON nd515.NDB_No = f.NDB_No AND nd515.Nutr_No = 515
    INNER JOIN [usda].NUTR_DEF nf515 ON nf515.Nutr_No = 515
    INNER JOIN [usda].NUT_DATA nd516 ON nd516.NDB_No = f.NDB_No AND nd516.Nutr_No = 516
    INNER JOIN [usda].NUTR_DEF nf516 ON nf516.Nutr_No = 516
    INNER JOIN [usda].NUT_DATA nd517 ON nd517.NDB_No = f.NDB_No AND nd517.Nutr_No = 517
    INNER JOIN [usda].NUTR_DEF nf517 ON nf517.Nutr_No = 517
    INNER JOIN [usda].NUT_DATA nd518 ON nd518.NDB_No = f.NDB_No AND nd518.Nutr_No = 518
    INNER JOIN [usda].NUTR_DEF nf518 ON nf518.Nutr_No = 518
    INNER JOIN [usda].NUT_DATA nd521 ON nd521.NDB_No = f.NDB_No AND nd521.Nutr_No = 521
    INNER JOIN [usda].NUTR_DEF nf521 ON nf521.Nutr_No = 521
    INNER JOIN [usda].NUT_DATA nd573 ON nd573.NDB_No = f.NDB_No AND nd573.Nutr_No = 573
    INNER JOIN [usda].NUTR_DEF nf573 ON nf573.Nutr_No = 573
    INNER JOIN [usda].NUT_DATA nd578 ON nd578.NDB_No = f.NDB_No AND nd578.Nutr_No = 578
    INNER JOIN [usda].NUTR_DEF nf578 ON nf578.Nutr_No = 578
    INNER JOIN [usda].NUT_DATA nd601 ON nd601.NDB_No = f.NDB_No AND nd601.Nutr_No = 601
    INNER JOIN [usda].NUTR_DEF nf601 ON nf601.Nutr_No = 601
    INNER JOIN [usda].NUT_DATA nd605 ON nd605.NDB_No = f.NDB_No AND nd605.Nutr_No = 605
    INNER JOIN [usda].NUTR_DEF nf605 ON nf605.Nutr_No = 605
    INNER JOIN [usda].NUT_DATA nd606 ON nd606.NDB_No = f.NDB_No AND nd606.Nutr_No = 606
    INNER JOIN [usda].NUTR_DEF nf606 ON nf606.Nutr_No = 606
    INNER JOIN [usda].NUT_DATA nd607 ON nd607.NDB_No = f.NDB_No AND nd607.Nutr_No = 607
    INNER JOIN [usda].NUTR_DEF nf607 ON nf607.Nutr_No = 607
    INNER JOIN [usda].NUT_DATA nd608 ON nd608.NDB_No = f.NDB_No AND nd608.Nutr_No = 608
    INNER JOIN [usda].NUTR_DEF nf608 ON nf608.Nutr_No = 608
    INNER JOIN [usda].NUT_DATA nd609 ON nd609.NDB_No = f.NDB_No AND nd609.Nutr_No = 609
    INNER JOIN [usda].NUTR_DEF nf609 ON nf609.Nutr_No = 609
    INNER JOIN [usda].NUT_DATA nd610 ON nd610.NDB_No = f.NDB_No AND nd610.Nutr_No = 610
    INNER JOIN [usda].NUTR_DEF nf610 ON nf610.Nutr_No = 610
    INNER JOIN [usda].NUT_DATA nd611 ON nd611.NDB_No = f.NDB_No AND nd611.Nutr_No = 611
    INNER JOIN [usda].NUTR_DEF nf611 ON nf611.Nutr_No = 611
    INNER JOIN [usda].NUT_DATA nd612 ON nd612.NDB_No = f.NDB_No AND nd612.Nutr_No = 612
    INNER JOIN [usda].NUTR_DEF nf612 ON nf612.Nutr_No = 612
    INNER JOIN [usda].NUT_DATA nd613 ON nd613.NDB_No = f.NDB_No AND nd613.Nutr_No = 613
    INNER JOIN [usda].NUTR_DEF nf613 ON nf613.Nutr_No = 613
    INNER JOIN [usda].NUT_DATA nd614 ON nd614.NDB_No = f.NDB_No AND nd614.Nutr_No = 614
    INNER JOIN [usda].NUTR_DEF nf614 ON nf614.Nutr_No = 614
    INNER JOIN [usda].NUT_DATA nd615 ON nd615.NDB_No = f.NDB_No AND nd615.Nutr_No = 615
    INNER JOIN [usda].NUTR_DEF nf615 ON nf615.Nutr_No = 615
    INNER JOIN [usda].NUT_DATA nd617 ON nd617.NDB_No = f.NDB_No AND nd617.Nutr_No = 617
    INNER JOIN [usda].NUTR_DEF nf617 ON nf617.Nutr_No = 617
    INNER JOIN [usda].NUT_DATA nd618 ON nd618.NDB_No = f.NDB_No AND nd618.Nutr_No = 618
    INNER JOIN [usda].NUTR_DEF nf618 ON nf618.Nutr_No = 618
    INNER JOIN [usda].NUT_DATA nd619 ON nd619.NDB_No = f.NDB_No AND nd619.Nutr_No = 619
    INNER JOIN [usda].NUTR_DEF nf619 ON nf619.Nutr_No = 619
    INNER JOIN [usda].NUT_DATA nd620 ON nd620.NDB_No = f.NDB_No AND nd620.Nutr_No = 620
    INNER JOIN [usda].NUTR_DEF nf620 ON nf620.Nutr_No = 620
    INNER JOIN [usda].NUT_DATA nd621 ON nd621.NDB_No = f.NDB_No AND nd621.Nutr_No = 621
    INNER JOIN [usda].NUTR_DEF nf621 ON nf621.Nutr_No = 621
    INNER JOIN [usda].NUT_DATA nd624 ON nd624.NDB_No = f.NDB_No AND nd624.Nutr_No = 624
    INNER JOIN [usda].NUTR_DEF nf624 ON nf624.Nutr_No = 624
    INNER JOIN [usda].NUT_DATA nd625 ON nd625.NDB_No = f.NDB_No AND nd625.Nutr_No = 625
    INNER JOIN [usda].NUTR_DEF nf625 ON nf625.Nutr_No = 625
    INNER JOIN [usda].NUT_DATA nd626 ON nd626.NDB_No = f.NDB_No AND nd626.Nutr_No = 626
    INNER JOIN [usda].NUTR_DEF nf626 ON nf626.Nutr_No = 626
    INNER JOIN [usda].NUT_DATA nd627 ON nd627.NDB_No = f.NDB_No AND nd627.Nutr_No = 627
    INNER JOIN [usda].NUTR_DEF nf627 ON nf627.Nutr_No = 627
    INNER JOIN [usda].NUT_DATA nd628 ON nd628.NDB_No = f.NDB_No AND nd628.Nutr_No = 628
    INNER JOIN [usda].NUTR_DEF nf628 ON nf628.Nutr_No = 628
    INNER JOIN [usda].NUT_DATA nd629 ON nd629.NDB_No = f.NDB_No AND nd629.Nutr_No = 629
    INNER JOIN [usda].NUTR_DEF nf629 ON nf629.Nutr_No = 629
    INNER JOIN [usda].NUT_DATA nd630 ON nd630.NDB_No = f.NDB_No AND nd630.Nutr_No = 630
    INNER JOIN [usda].NUTR_DEF nf630 ON nf630.Nutr_No = 630
    INNER JOIN [usda].NUT_DATA nd631 ON nd631.NDB_No = f.NDB_No AND nd631.Nutr_No = 631
    INNER JOIN [usda].NUTR_DEF nf631 ON nf631.Nutr_No = 631
    INNER JOIN [usda].NUT_DATA nd636 ON nd636.NDB_No = f.NDB_No AND nd636.Nutr_No = 636
    INNER JOIN [usda].NUTR_DEF nf636 ON nf636.Nutr_No = 636
    INNER JOIN [usda].NUT_DATA nd638 ON nd638.NDB_No = f.NDB_No AND nd638.Nutr_No = 638
    INNER JOIN [usda].NUTR_DEF nf638 ON nf638.Nutr_No = 638
    INNER JOIN [usda].NUT_DATA nd639 ON nd639.NDB_No = f.NDB_No AND nd639.Nutr_No = 639
    INNER JOIN [usda].NUTR_DEF nf639 ON nf639.Nutr_No = 639
    INNER JOIN [usda].NUT_DATA nd641 ON nd641.NDB_No = f.NDB_No AND nd641.Nutr_No = 641
    INNER JOIN [usda].NUTR_DEF nf641 ON nf641.Nutr_No = 641
    INNER JOIN [usda].NUT_DATA nd645 ON nd645.NDB_No = f.NDB_No AND nd645.Nutr_No = 645
    INNER JOIN [usda].NUTR_DEF nf645 ON nf645.Nutr_No = 645
    INNER JOIN [usda].NUT_DATA nd646 ON nd646.NDB_No = f.NDB_No AND nd646.Nutr_No = 646
    INNER JOIN [usda].NUTR_DEF nf646 ON nf646.Nutr_No = 646
    INNER JOIN [usda].NUT_DATA nd652 ON nd652.NDB_No = f.NDB_No AND nd652.Nutr_No = 652
    INNER JOIN [usda].NUTR_DEF nf652 ON nf652.Nutr_No = 652
    INNER JOIN [usda].NUT_DATA nd653 ON nd653.NDB_No = f.NDB_No AND nd653.Nutr_No = 653
    INNER JOIN [usda].NUTR_DEF nf653 ON nf653.Nutr_No = 653
    INNER JOIN [usda].NUT_DATA nd654 ON nd654.NDB_No = f.NDB_No AND nd654.Nutr_No = 654
    INNER JOIN [usda].NUTR_DEF nf654 ON nf654.Nutr_No = 654
    INNER JOIN [usda].NUT_DATA nd662 ON nd662.NDB_No = f.NDB_No AND nd662.Nutr_No = 662
    INNER JOIN [usda].NUTR_DEF nf662 ON nf662.Nutr_No = 662
    INNER JOIN [usda].NUT_DATA nd663 ON nd663.NDB_No = f.NDB_No AND nd663.Nutr_No = 663
    INNER JOIN [usda].NUTR_DEF nf663 ON nf663.Nutr_No = 663
    INNER JOIN [usda].NUT_DATA nd664 ON nd664.NDB_No = f.NDB_No AND nd664.Nutr_No = 664
    INNER JOIN [usda].NUTR_DEF nf664 ON nf664.Nutr_No = 664
    INNER JOIN [usda].NUT_DATA nd665 ON nd665.NDB_No = f.NDB_No AND nd665.Nutr_No = 665
    INNER JOIN [usda].NUTR_DEF nf665 ON nf665.Nutr_No = 665
    INNER JOIN [usda].NUT_DATA nd666 ON nd666.NDB_No = f.NDB_No AND nd666.Nutr_No = 666
    INNER JOIN [usda].NUTR_DEF nf666 ON nf666.Nutr_No = 666
    INNER JOIN [usda].NUT_DATA nd669 ON nd669.NDB_No = f.NDB_No AND nd669.Nutr_No = 669
    INNER JOIN [usda].NUTR_DEF nf669 ON nf669.Nutr_No = 669
    INNER JOIN [usda].NUT_DATA nd670 ON nd670.NDB_No = f.NDB_No AND nd670.Nutr_No = 670
    INNER JOIN [usda].NUTR_DEF nf670 ON nf670.Nutr_No = 670
    INNER JOIN [usda].NUT_DATA nd671 ON nd671.NDB_No = f.NDB_No AND nd671.Nutr_No = 671
    INNER JOIN [usda].NUTR_DEF nf671 ON nf671.Nutr_No = 671
    INNER JOIN [usda].NUT_DATA nd672 ON nd672.NDB_No = f.NDB_No AND nd672.Nutr_No = 672
    INNER JOIN [usda].NUTR_DEF nf672 ON nf672.Nutr_No = 672
    INNER JOIN [usda].NUT_DATA nd673 ON nd673.NDB_No = f.NDB_No AND nd673.Nutr_No = 673
    INNER JOIN [usda].NUTR_DEF nf673 ON nf673.Nutr_No = 673
    INNER JOIN [usda].NUT_DATA nd674 ON nd674.NDB_No = f.NDB_No AND nd674.Nutr_No = 674
    INNER JOIN [usda].NUTR_DEF nf674 ON nf674.Nutr_No = 674
    INNER JOIN [usda].NUT_DATA nd675 ON nd675.NDB_No = f.NDB_No AND nd675.Nutr_No = 675
    INNER JOIN [usda].NUTR_DEF nf675 ON nf675.Nutr_No = 675
    INNER JOIN [usda].NUT_DATA nd676 ON nd676.NDB_No = f.NDB_No AND nd676.Nutr_No = 676
    INNER JOIN [usda].NUTR_DEF nf676 ON nf676.Nutr_No = 676
    INNER JOIN [usda].NUT_DATA nd685 ON nd685.NDB_No = f.NDB_No AND nd685.Nutr_No = 685
    INNER JOIN [usda].NUTR_DEF nf685 ON nf685.Nutr_No = 685
    INNER JOIN [usda].NUT_DATA nd687 ON nd687.NDB_No = f.NDB_No AND nd687.Nutr_No = 687
    INNER JOIN [usda].NUTR_DEF nf687 ON nf687.Nutr_No = 687
    INNER JOIN [usda].NUT_DATA nd689 ON nd689.NDB_No = f.NDB_No AND nd689.Nutr_No = 689
    INNER JOIN [usda].NUTR_DEF nf689 ON nf689.Nutr_No = 689
    INNER JOIN [usda].NUT_DATA nd693 ON nd693.NDB_No = f.NDB_No AND nd693.Nutr_No = 693
    INNER JOIN [usda].NUTR_DEF nf693 ON nf693.Nutr_No = 693
    INNER JOIN [usda].NUT_DATA nd695 ON nd695.NDB_No = f.NDB_No AND nd695.Nutr_No = 695
    INNER JOIN [usda].NUTR_DEF nf695 ON nf695.Nutr_No = 695
    INNER JOIN [usda].NUT_DATA nd696 ON nd696.NDB_No = f.NDB_No AND nd696.Nutr_No = 696
    INNER JOIN [usda].NUTR_DEF nf696 ON nf696.Nutr_No = 696
    INNER JOIN [usda].NUT_DATA nd697 ON nd697.NDB_No = f.NDB_No AND nd697.Nutr_No = 697
    INNER JOIN [usda].NUTR_DEF nf697 ON nf697.Nutr_No = 697
    INNER JOIN [usda].NUT_DATA nd851 ON nd851.NDB_No = f.NDB_No AND nd851.Nutr_No = 851
    INNER JOIN [usda].NUTR_DEF nf851 ON nf851.Nutr_No = 851
    INNER JOIN [usda].NUT_DATA nd852 ON nd852.NDB_No = f.NDB_No AND nd852.Nutr_No = 852
    INNER JOIN [usda].NUTR_DEF nf852 ON nf852.Nutr_No = 852
    INNER JOIN [usda].NUT_DATA nd853 ON nd853.NDB_No = f.NDB_No AND nd853.Nutr_No = 853
    INNER JOIN [usda].NUTR_DEF nf853 ON nf853.Nutr_No = 853
    INNER JOIN [usda].NUT_DATA nd855 ON nd855.NDB_No = f.NDB_No AND nd855.Nutr_No = 855
    INNER JOIN [usda].NUTR_DEF nf855 ON nf855.Nutr_No = 855
    INNER JOIN [usda].NUT_DATA nd856 ON nd856.NDB_No = f.NDB_No AND nd856.Nutr_No = 856
    INNER JOIN [usda].NUTR_DEF nf856 ON nf856.Nutr_No = 856
    INNER JOIN [usda].NUT_DATA nd857 ON nd857.NDB_No = f.NDB_No AND nd857.Nutr_No = 857
    INNER JOIN [usda].NUTR_DEF nf857 ON nf857.Nutr_No = 857
    INNER JOIN [usda].NUT_DATA nd858 ON nd858.NDB_No = f.NDB_No AND nd858.Nutr_No = 858
    INNER JOIN [usda].NUTR_DEF nf858 ON nf858.Nutr_No = 858
    INNER JOIN [usda].NUT_DATA nd859 ON nd859.NDB_No = f.NDB_No AND nd859.Nutr_No = 859
    INNER JOIN [usda].NUTR_DEF nf859 ON nf859.Nutr_No = 859
ORDER BY fd.FdGrp_Desc, f.Long_Desc