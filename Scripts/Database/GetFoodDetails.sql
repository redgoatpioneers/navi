USE ProtoNaviDb;

SELECT f.Description, nt.Name, fn.Amount, nt.Unit
FROM Foods f
INNER JOIN FoodNutrients fn
	ON f.FoodId = fn.FoodId
INNER JOIN NutrientTypes nt
	ON fn.NutrientTypeId = nt.NutrientTypeId
--WHERE f.Description LIKE '%brocolli%'
ORDER BY f.Description, nt.Name