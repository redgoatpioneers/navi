USE ProtoNaviDb;

WITH x AS (
	SELECT DISTINCT
		f.NDB_No, g.FdGrp_Desc, f.Long_Desc,
		np.Nutr_Val FoodProtein,
		nf.Nutr_Val FoodFat,
		nc.Nutr_Val FoodCarbs,
		nk.Nutr_Val FoodCalories,
		w.Amount, w.Msre_Desc, w.Gm_Wgt,
		(w.Gm_Wgt / w.Amount) UnitMass,
		(w.Gm_Wgt / w.Amount) * CASE
			WHEN w.Msre_Desc = 'cup' THEN 1
			WHEN w.Msre_Desc = 'fl oz' THEN 8
			WHEN w.Msre_Desc = 'oz' THEN 8
			ELSE 0 END AS NormalizedMass
	FROM usda.FOOD_DES f
		INNER JOIN usda.FD_GROUP g ON f.FdGrp_Cd = g.FdGrp_Cd
		INNER JOIN usda.NUT_DATA np ON f.NDB_No = np.NDB_No AND np.Nutr_No = '203'
		INNER JOIN usda.NUT_DATA nf ON f.NDB_No = nf.NDB_No AND nf.Nutr_No = '204'
		INNER JOIN usda.NUT_DATA nc ON f.NDB_No = nc.NDB_No AND nc.Nutr_No = '205'
		INNER JOIN usda.NUT_DATA nk ON f.NDB_No = nk.NDB_No AND nk.Nutr_No = '208'
		INNER JOIN usda.WEIGHT w ON f.NDB_No = w.NDB_No
	WHERE
		f.IsActive = 1 AND w.IsActive = 1
		--AND f.NDB_No IN (12643, 12011, 11011, 9362, 6416)
		AND w.Msre_Desc IN ('cup', 'fl oz', 'oz')
)
SELECT
	x.NDB_No ID,
	x.FdGrp_Desc Category,
	x.Long_Desc Description,
	CONVERT(DECIMAL(10, 2), x.FoodProtein * (x.UnitMass/100)) "Protein (g/g)",
	CONVERT(DECIMAL(10, 2), x.FoodFat * (x.UnitMass/100)) "Fat (g/g)",
	CONVERT(DECIMAL(10, 2), x.FoodCarbs * (x.UnitMass/100)) "Carbs (g/g)",
	CONVERT(DECIMAL(10, 2), x.Amount) "USDA Reported Amount",
	x.Msre_Desc "Reported Unit of Measure (UoM)",
	CONVERT(DECIMAL(10, 2), x.Gm_Wgt) "Total Component Mass (g)",
	CONVERT(DECIMAL(10, 2), x.UnitMass) "Initial (g/UoM)",
	CONVERT(DECIMAL(10, 2), x.NormalizedMass) "Normalized (g/cup)",
	CONVERT(DECIMAL(10, 2), x.FoodProtein * (x.NormalizedMass / 100)) "Protein (g/cup)",
	CONVERT(DECIMAL(10, 2), x.FoodProtein * (x.NormalizedMass / 100) / 16) "Protein (g/tbsp)",
	CONVERT(DECIMAL(10, 2), x.FoodProtein * (x.NormalizedMass / 100) / 48) "Protein (g/tsp)",
	CONVERT(DECIMAL(10, 2), x.FoodFat * (x.NormalizedMass / 100)) "Fat (g/cup)",
	CONVERT(DECIMAL(10, 2), x.FoodFat * (x.NormalizedMass / 100) / 16) "Fat (g/tbsp)",
	CONVERT(DECIMAL(10, 2), x.FoodFat * (x.NormalizedMass / 100) / 48) "Fat (g/tsp)",
	CONVERT(DECIMAL(10, 2), x.FoodCarbs * (x.NormalizedMass / 100)) "Carbs (g/cup)",
	CONVERT(DECIMAL(10, 2), x.FoodCarbs * (x.NormalizedMass / 100) / 16) "Carbs (g/tbsp)",
	CONVERT(DECIMAL(10, 2), x.FoodCarbs * (x.NormalizedMass / 100) / 48) "Carbs (g/tsp)",
	CONVERT(DECIMAL(10, 2), x.FoodCalories / 100) "(kcal/g)",
	CONVERT(DECIMAL(10, 2), x.FoodCalories * (x.NormalizedMass / 100)) "(kcal/cup)",
	CONVERT(DECIMAL(10, 2), x.FoodCalories * (x.NormalizedMass / 100) / 16) "(kcal/tbsp)",
	CONVERT(DECIMAL(10, 2), x.FoodCalories * (x.NormalizedMass / 100) / 48) "(kcal/tsp)"
FROM x
ORDER BY x.NDB_No, x.UnitMass