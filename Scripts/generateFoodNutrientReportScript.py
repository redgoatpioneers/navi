import sys, os

outdir = os.path.join(os.path.dirname(os.path.realpath(__file__)))
if not os.path.exists(outdir):
    os.makedirs(outdir)

# SELECT DISTINCT
#     fd.FdGrp_Desc Category, f.NDB_No USDAFoodID, f.Long_Desc, f.Shrt_Desc,
#     n.NutrDesc Nutrient, CONCAT(nd.Nutr_Val, ' ', n.Units) Amount
# FROM [usda].FOOD_DES f
#     INNER JOIN [usda].NUT_DATA nd ON f.NDB_No = nd.NDB_No
#     INNER JOIN [usda].NUTR_DEF n ON nd.Nutr_No = n.Nutr_No
#     INNER JOIN [usda].FD_GROUP fd ON f.FdGrp_Cd = fd.FdGrp_Cd
# ORDER BY fd.FdGrp_Desc, f.Long_Desc, n.NutrDesc

outfile = os.path.join(outdir, "generateFoodNutrientReportScript.sql")
with open(outfile,"w+") as outfile:
    select_start = "SELECT DISTINCT fd.FdGrp_Desc Category, f.NDB_No USDAFoodID, f.Long_Desc, f.Shrt_Desc,"
    nutrients = "203;204;205;207;208;209;210;211;212;213;214;221;255;262;263;268;269;287;291;301;303;304;305;306;307;309;312;313;315;317;318;319;320;321;322;323;324;325;326;328;334;337;338;341;342;343;344;345;346;347;401;404;405;406;410;415;417;418;421;428;429;430;431;432;435;454;501;502;503;504;505;506;507;508;509;510;511;512;513;514;515;516;517;518;521;573;578;601;605;606;607;608;609;610;611;612;613;614;615;617;618;619;620;621;624;625;626;627;628;629;630;631;636;638;639;641;645;646;652;653;654;662;663;664;665;666;669;670;671;672;673;674;675;676;685;687;689;693;695;696;697;851;852;853;855;856;857;858;859".split(";")
    nutr_desc = u"Protein (g);Total lipid (fat) (g);Carbohydrate, by difference (g);Ash (g);Energy (kcal);Starch (g);Sucrose (g);Glucose (dextrose) (g);Fructose (g);Lactose (g);Maltose (g);Alcohol, ethyl (g);Water (g);Caffeine (mg);Theobromine (mg);Energy (kJ);Sugars, total (g);Galactose (g);Fiber, total dietary (g);Calcium, Ca (mg);Iron, Fe (mg);Magnesium, Mg (mg);Phosphorus, P (mg);Potassium, K (mg);Sodium, Na (mg);Zinc, Zn (mg);Copper, Cu (mg);Fluoride, F (µg);Manganese, Mn (mg);Selenium, Se (µg);Vitamin A, IU (IU);Retinol (µg);Vitamin A, RAE (µg);Carotene, beta (µg);Carotene, alpha (µg);Vitamin E (alpha-tocopherol) (mg);Vitamin D (IU);Vitamin D2 (ergocalciferol) (µg);Vitamin D3 (cholecalciferol) (µg);Vitamin D (D2 + D3) (µg);Cryptoxanthin, beta (µg);Lycopene (µg);Lutein + zeaxanthin (µg);Tocopherol, beta (mg);Tocopherol, gamma (mg);Tocopherol, delta (mg);Tocotrienol, alpha (mg);Tocotrienol, beta (mg);Tocotrienol, gamma (mg);Tocotrienol, delta (mg);Vitamin C, total ascorbic acid (mg);Thiamin (mg);Riboflavin (mg);Niacin (mg);Pantothenic acid (mg);Vitamin B-6 (mg);Folate, total (µg);Vitamin B-12 (µg);Choline, total (mg);Menaquinone-4 (µg);Dihydrophylloquinone (µg);Vitamin K (phylloquinone) (µg);Folic acid (µg);Folate, food (µg);Folate, DFE (µg);Betaine (mg);Tryptophan (g);Threonine (g);Isoleucine (g);Leucine (g);Lysine (g);Methionine (g);Cystine (g);Phenylalanine (g);Tyrosine (g);Valine (g);Arginine (g);Histidine (g);Alanine (g);Aspartic acid (g);Glutamic acid (g);Glycine (g);Proline (g);Serine (g);Hydroxyproline (g);Vitamin E, added (mg);Vitamin B-12, added (µg);Cholesterol (mg);Fatty acids, total trans (g);Fatty acids, total saturated (g);4:0 (g);6:0 (g);8:0 (g);10:0 (g);12:0 (g);14:0 (g);16:0 (g);18:0 (g);20:0 (g);18:1 undifferentiated (g);18:2 undifferentiated (g);18:3 undifferentiated (g);20:4 undifferentiated (g);22:6 n-3 (DHA) (g);22:0 (g);14:1 (g);16:1 undifferentiated (g);18:4 (g);20:1 (g);20:5 n-3 (EPA) (g);22:1 undifferentiated (g);22:5 n-3 (DPA) (g);Phytosterols (mg);Stigmasterol (mg);Campesterol (mg);Beta-sitosterol (mg);Fatty acids, total monounsaturated (g);Fatty acids, total polyunsaturated (g);15:0 (g);17:0 (g);24:0 (g);16:1 t (g);18:1 t (g);22:1 t (g);18:2 t not further defined (g);18:2 i (g);18:2 t,t (g);18:2 CLAs (g);24:1 c (g);20:2 n-6 c,c (g);16:1 c (g);18:1 c (g);18:2 n-6 c,c (g);22:1 c (g);18:3 n-6 c,c,c (g);17:1 (g);20:3 undifferentiated (g);Fatty acids, total trans-monoenoic (g);Fatty acids, total trans-polyenoic (g);13:0 (g);15:1 (g);18:3 n-3 c,c,c (ALA) (g);20:3 n-3 (g);20:3 n-6 (g);20:4 n-6 (g);18:3i (g);21:5 (g);22:4 (g);18:1-11 t (18:1t n-7) (g)".split(";")
    select_lines = ", ".join(("\n    nd{0}.Nutr_Val \"{1}\"".format(nutrient, nutr_desc) for nutrient, nutr_desc in zip(nutrients, nutr_desc)))
    from_start = "\nFROM [usda].FOOD_DES f INNER JOIN [usda].FD_GROUP fd ON f.FdGrp_Cd = fd.FdGrp_Cd"
    join_lines = ("\n    LEFT JOIN [usda].NUT_DATA nd{0} ON nd{0}.NDB_No = f.NDB_No AND nd{0}.Nutr_No = {0}\n    LEFT JOIN [usda].NUTR_DEF nf{0} ON nf{0}.Nutr_No = {0}".format(nutrient) for nutrient in nutrients)
    script_end = "\nORDER BY fd.FdGrp_Desc, f.Long_Desc"
    outfile.writelines(select_start)
    outfile.writelines(select_lines)
    outfile.writelines(from_start)
    outfile.writelines(join_lines)
    outfile.writelines(script_end)
