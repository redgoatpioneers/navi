SELECT DISTINCT fd.FdGrp_Desc Category, f.NDB_No USDAFoodID, f.Long_Desc, f.Shrt_Desc,
    nd203.Nutr_Val "Protein (g)", 
    nd204.Nutr_Val "Total lipid (fat) (g)", 
    nd205.Nutr_Val "Carbohydrate, by difference (g)", 
    nd207.Nutr_Val "Ash (g)", 
    nd208.Nutr_Val "Energy (kcal)", 
    nd209.Nutr_Val "Starch (g)", 
    nd210.Nutr_Val "Sucrose (g)", 
    nd211.Nutr_Val "Glucose (dextrose) (g)", 
    nd212.Nutr_Val "Fructose (g)", 
    nd213.Nutr_Val "Lactose (g)", 
    nd214.Nutr_Val "Maltose (g)", 
    nd221.Nutr_Val "Alcohol, ethyl (g)", 
    nd255.Nutr_Val "Water (g)", 
    nd262.Nutr_Val "Caffeine (mg)", 
    nd263.Nutr_Val "Theobromine (mg)", 
    nd268.Nutr_Val "Energy (kJ)", 
    nd269.Nutr_Val "Sugars, total (g)", 
    nd287.Nutr_Val "Galactose (g)", 
    nd291.Nutr_Val "Fiber, total dietary (g)", 
    nd301.Nutr_Val "Calcium, Ca (mg)", 
    nd303.Nutr_Val "Iron, Fe (mg)", 
    nd304.Nutr_Val "Magnesium, Mg (mg)", 
    nd305.Nutr_Val "Phosphorus, P (mg)", 
    nd306.Nutr_Val "Potassium, K (mg)", 
    nd307.Nutr_Val "Sodium, Na (mg)", 
    nd309.Nutr_Val "Zinc, Zn (mg)", 
    nd312.Nutr_Val "Copper, Cu (mg)", 
    nd313.Nutr_Val "Fluoride, F (�g)", 
    nd315.Nutr_Val "Manganese, Mn (mg)", 
    nd317.Nutr_Val "Selenium, Se (�g)", 
    nd318.Nutr_Val "Vitamin A, IU (IU)", 
    nd319.Nutr_Val "Retinol (�g)", 
    nd320.Nutr_Val "Vitamin A, RAE (�g)", 
    nd321.Nutr_Val "Carotene, beta (�g)", 
    nd322.Nutr_Val "Carotene, alpha (�g)", 
    nd323.Nutr_Val "Vitamin E (alpha-tocopherol) (mg)", 
    nd324.Nutr_Val "Vitamin D (IU)", 
    nd325.Nutr_Val "Vitamin D2 (ergocalciferol) (�g)", 
    nd326.Nutr_Val "Vitamin D3 (cholecalciferol) (�g)", 
    nd328.Nutr_Val "Vitamin D (D2 + D3) (�g)", 
    nd334.Nutr_Val "Cryptoxanthin, beta (�g)", 
    nd337.Nutr_Val "Lycopene (�g)", 
    nd338.Nutr_Val "Lutein + zeaxanthin (�g)", 
    nd341.Nutr_Val "Tocopherol, beta (mg)", 
    nd342.Nutr_Val "Tocopherol, gamma (mg)", 
    nd343.Nutr_Val "Tocopherol, delta (mg)", 
    nd344.Nutr_Val "Tocotrienol, alpha (mg)", 
    nd345.Nutr_Val "Tocotrienol, beta (mg)", 
    nd346.Nutr_Val "Tocotrienol, gamma (mg)", 
    nd347.Nutr_Val "Tocotrienol, delta (mg)", 
    nd401.Nutr_Val "Vitamin C, total ascorbic acid (mg)", 
    nd404.Nutr_Val "Thiamin (mg)", 
    nd405.Nutr_Val "Riboflavin (mg)", 
    nd406.Nutr_Val "Niacin (mg)", 
    nd410.Nutr_Val "Pantothenic acid (mg)", 
    nd415.Nutr_Val "Vitamin B-6 (mg)", 
    nd417.Nutr_Val "Folate, total (�g)", 
    nd418.Nutr_Val "Vitamin B-12 (�g)", 
    nd421.Nutr_Val "Choline, total (mg)", 
    nd428.Nutr_Val "Menaquinone-4 (�g)", 
    nd429.Nutr_Val "Dihydrophylloquinone (�g)", 
    nd430.Nutr_Val "Vitamin K (phylloquinone) (�g)", 
    nd431.Nutr_Val "Folic acid (�g)", 
    nd432.Nutr_Val "Folate, food (�g)", 
    nd435.Nutr_Val "Folate, DFE (�g)", 
    nd454.Nutr_Val "Betaine (mg)", 
    nd501.Nutr_Val "Tryptophan (g)", 
    nd502.Nutr_Val "Threonine (g)", 
    nd503.Nutr_Val "Isoleucine (g)", 
    nd504.Nutr_Val "Leucine (g)", 
    nd505.Nutr_Val "Lysine (g)", 
    nd506.Nutr_Val "Methionine (g)", 
    nd507.Nutr_Val "Cystine (g)", 
    nd508.Nutr_Val "Phenylalanine (g)", 
    nd509.Nutr_Val "Tyrosine (g)", 
    nd510.Nutr_Val "Valine (g)", 
    nd511.Nutr_Val "Arginine (g)", 
    nd512.Nutr_Val "Histidine (g)", 
    nd513.Nutr_Val "Alanine (g)", 
    nd514.Nutr_Val "Aspartic acid (g)", 
    nd515.Nutr_Val "Glutamic acid (g)", 
    nd516.Nutr_Val "Glycine (g)", 
    nd517.Nutr_Val "Proline (g)", 
    nd518.Nutr_Val "Serine (g)", 
    nd521.Nutr_Val "Hydroxyproline (g)", 
    nd573.Nutr_Val "Vitamin E, added (mg)", 
    nd578.Nutr_Val "Vitamin B-12, added (�g)", 
    nd601.Nutr_Val "Cholesterol (mg)", 
    nd605.Nutr_Val "Fatty acids, total trans (g)", 
    nd606.Nutr_Val "Fatty acids, total saturated (g)", 
    nd607.Nutr_Val "4:0 (g)", 
    nd608.Nutr_Val "6:0 (g)", 
    nd609.Nutr_Val "8:0 (g)", 
    nd610.Nutr_Val "10:0 (g)", 
    nd611.Nutr_Val "12:0 (g)", 
    nd612.Nutr_Val "14:0 (g)", 
    nd613.Nutr_Val "16:0 (g)", 
    nd614.Nutr_Val "18:0 (g)", 
    nd615.Nutr_Val "20:0 (g)", 
    nd617.Nutr_Val "18:1 undifferentiated (g)", 
    nd618.Nutr_Val "18:2 undifferentiated (g)", 
    nd619.Nutr_Val "18:3 undifferentiated (g)", 
    nd620.Nutr_Val "20:4 undifferentiated (g)", 
    nd621.Nutr_Val "22:6 n-3 (DHA) (g)", 
    nd624.Nutr_Val "22:0 (g)", 
    nd625.Nutr_Val "14:1 (g)", 
    nd626.Nutr_Val "16:1 undifferentiated (g)", 
    nd627.Nutr_Val "18:4 (g)", 
    nd628.Nutr_Val "20:1 (g)", 
    nd629.Nutr_Val "20:5 n-3 (EPA) (g)", 
    nd630.Nutr_Val "22:1 undifferentiated (g)", 
    nd631.Nutr_Val "22:5 n-3 (DPA) (g)", 
    nd636.Nutr_Val "Phytosterols (mg)", 
    nd638.Nutr_Val "Stigmasterol (mg)", 
    nd639.Nutr_Val "Campesterol (mg)", 
    nd641.Nutr_Val "Beta-sitosterol (mg)", 
    nd645.Nutr_Val "Fatty acids, total monounsaturated (g)", 
    nd646.Nutr_Val "Fatty acids, total polyunsaturated (g)", 
    nd652.Nutr_Val "15:0 (g)", 
    nd653.Nutr_Val "17:0 (g)", 
    nd654.Nutr_Val "24:0 (g)", 
    nd662.Nutr_Val "16:1 t (g)", 
    nd663.Nutr_Val "18:1 t (g)", 
    nd664.Nutr_Val "22:1 t (g)", 
    nd665.Nutr_Val "18:2 t not further defined (g)", 
    nd666.Nutr_Val "18:2 i (g)", 
    nd669.Nutr_Val "18:2 t,t (g)", 
    nd670.Nutr_Val "18:2 CLAs (g)", 
    nd671.Nutr_Val "24:1 c (g)", 
    nd672.Nutr_Val "20:2 n-6 c,c (g)", 
    nd673.Nutr_Val "16:1 c (g)", 
    nd674.Nutr_Val "18:1 c (g)", 
    nd675.Nutr_Val "18:2 n-6 c,c (g)", 
    nd676.Nutr_Val "22:1 c (g)", 
    nd685.Nutr_Val "18:3 n-6 c,c,c (g)", 
    nd687.Nutr_Val "17:1 (g)", 
    nd689.Nutr_Val "20:3 undifferentiated (g)", 
    nd693.Nutr_Val "Fatty acids, total trans-monoenoic (g)", 
    nd695.Nutr_Val "Fatty acids, total trans-polyenoic (g)", 
    nd696.Nutr_Val "13:0 (g)", 
    nd697.Nutr_Val "15:1 (g)", 
    nd851.Nutr_Val "18:3 n-3 c,c,c (ALA) (g)", 
    nd852.Nutr_Val "20:3 n-3 (g)", 
    nd853.Nutr_Val "20:3 n-6 (g)", 
    nd855.Nutr_Val "20:4 n-6 (g)", 
    nd856.Nutr_Val "18:3i (g)", 
    nd857.Nutr_Val "21:5 (g)", 
    nd858.Nutr_Val "22:4 (g)", 
    nd859.Nutr_Val "18:1-11 t (18:1t n-7) (g)"
FROM [usda].FOOD_DES f INNER JOIN [usda].FD_GROUP fd ON f.FdGrp_Cd = fd.FdGrp_Cd
    LEFT JOIN [usda].NUT_DATA nd203 ON nd203.NDB_No = f.NDB_No AND nd203.Nutr_No = 203
    LEFT JOIN [usda].NUTR_DEF nf203 ON nf203.Nutr_No = 203
    LEFT JOIN [usda].NUT_DATA nd204 ON nd204.NDB_No = f.NDB_No AND nd204.Nutr_No = 204
    LEFT JOIN [usda].NUTR_DEF nf204 ON nf204.Nutr_No = 204
    LEFT JOIN [usda].NUT_DATA nd205 ON nd205.NDB_No = f.NDB_No AND nd205.Nutr_No = 205
    LEFT JOIN [usda].NUTR_DEF nf205 ON nf205.Nutr_No = 205
    LEFT JOIN [usda].NUT_DATA nd207 ON nd207.NDB_No = f.NDB_No AND nd207.Nutr_No = 207
    LEFT JOIN [usda].NUTR_DEF nf207 ON nf207.Nutr_No = 207
    LEFT JOIN [usda].NUT_DATA nd208 ON nd208.NDB_No = f.NDB_No AND nd208.Nutr_No = 208
    LEFT JOIN [usda].NUTR_DEF nf208 ON nf208.Nutr_No = 208
    LEFT JOIN [usda].NUT_DATA nd209 ON nd209.NDB_No = f.NDB_No AND nd209.Nutr_No = 209
    LEFT JOIN [usda].NUTR_DEF nf209 ON nf209.Nutr_No = 209
    LEFT JOIN [usda].NUT_DATA nd210 ON nd210.NDB_No = f.NDB_No AND nd210.Nutr_No = 210
    LEFT JOIN [usda].NUTR_DEF nf210 ON nf210.Nutr_No = 210
    LEFT JOIN [usda].NUT_DATA nd211 ON nd211.NDB_No = f.NDB_No AND nd211.Nutr_No = 211
    LEFT JOIN [usda].NUTR_DEF nf211 ON nf211.Nutr_No = 211
    LEFT JOIN [usda].NUT_DATA nd212 ON nd212.NDB_No = f.NDB_No AND nd212.Nutr_No = 212
    LEFT JOIN [usda].NUTR_DEF nf212 ON nf212.Nutr_No = 212
    LEFT JOIN [usda].NUT_DATA nd213 ON nd213.NDB_No = f.NDB_No AND nd213.Nutr_No = 213
    LEFT JOIN [usda].NUTR_DEF nf213 ON nf213.Nutr_No = 213
    LEFT JOIN [usda].NUT_DATA nd214 ON nd214.NDB_No = f.NDB_No AND nd214.Nutr_No = 214
    LEFT JOIN [usda].NUTR_DEF nf214 ON nf214.Nutr_No = 214
    LEFT JOIN [usda].NUT_DATA nd221 ON nd221.NDB_No = f.NDB_No AND nd221.Nutr_No = 221
    LEFT JOIN [usda].NUTR_DEF nf221 ON nf221.Nutr_No = 221
    LEFT JOIN [usda].NUT_DATA nd255 ON nd255.NDB_No = f.NDB_No AND nd255.Nutr_No = 255
    LEFT JOIN [usda].NUTR_DEF nf255 ON nf255.Nutr_No = 255
    LEFT JOIN [usda].NUT_DATA nd262 ON nd262.NDB_No = f.NDB_No AND nd262.Nutr_No = 262
    LEFT JOIN [usda].NUTR_DEF nf262 ON nf262.Nutr_No = 262
    LEFT JOIN [usda].NUT_DATA nd263 ON nd263.NDB_No = f.NDB_No AND nd263.Nutr_No = 263
    LEFT JOIN [usda].NUTR_DEF nf263 ON nf263.Nutr_No = 263
    LEFT JOIN [usda].NUT_DATA nd268 ON nd268.NDB_No = f.NDB_No AND nd268.Nutr_No = 268
    LEFT JOIN [usda].NUTR_DEF nf268 ON nf268.Nutr_No = 268
    LEFT JOIN [usda].NUT_DATA nd269 ON nd269.NDB_No = f.NDB_No AND nd269.Nutr_No = 269
    LEFT JOIN [usda].NUTR_DEF nf269 ON nf269.Nutr_No = 269
    LEFT JOIN [usda].NUT_DATA nd287 ON nd287.NDB_No = f.NDB_No AND nd287.Nutr_No = 287
    LEFT JOIN [usda].NUTR_DEF nf287 ON nf287.Nutr_No = 287
    LEFT JOIN [usda].NUT_DATA nd291 ON nd291.NDB_No = f.NDB_No AND nd291.Nutr_No = 291
    LEFT JOIN [usda].NUTR_DEF nf291 ON nf291.Nutr_No = 291
    LEFT JOIN [usda].NUT_DATA nd301 ON nd301.NDB_No = f.NDB_No AND nd301.Nutr_No = 301
    LEFT JOIN [usda].NUTR_DEF nf301 ON nf301.Nutr_No = 301
    LEFT JOIN [usda].NUT_DATA nd303 ON nd303.NDB_No = f.NDB_No AND nd303.Nutr_No = 303
    LEFT JOIN [usda].NUTR_DEF nf303 ON nf303.Nutr_No = 303
    LEFT JOIN [usda].NUT_DATA nd304 ON nd304.NDB_No = f.NDB_No AND nd304.Nutr_No = 304
    LEFT JOIN [usda].NUTR_DEF nf304 ON nf304.Nutr_No = 304
    LEFT JOIN [usda].NUT_DATA nd305 ON nd305.NDB_No = f.NDB_No AND nd305.Nutr_No = 305
    LEFT JOIN [usda].NUTR_DEF nf305 ON nf305.Nutr_No = 305
    LEFT JOIN [usda].NUT_DATA nd306 ON nd306.NDB_No = f.NDB_No AND nd306.Nutr_No = 306
    LEFT JOIN [usda].NUTR_DEF nf306 ON nf306.Nutr_No = 306
    LEFT JOIN [usda].NUT_DATA nd307 ON nd307.NDB_No = f.NDB_No AND nd307.Nutr_No = 307
    LEFT JOIN [usda].NUTR_DEF nf307 ON nf307.Nutr_No = 307
    LEFT JOIN [usda].NUT_DATA nd309 ON nd309.NDB_No = f.NDB_No AND nd309.Nutr_No = 309
    LEFT JOIN [usda].NUTR_DEF nf309 ON nf309.Nutr_No = 309
    LEFT JOIN [usda].NUT_DATA nd312 ON nd312.NDB_No = f.NDB_No AND nd312.Nutr_No = 312
    LEFT JOIN [usda].NUTR_DEF nf312 ON nf312.Nutr_No = 312
    LEFT JOIN [usda].NUT_DATA nd313 ON nd313.NDB_No = f.NDB_No AND nd313.Nutr_No = 313
    LEFT JOIN [usda].NUTR_DEF nf313 ON nf313.Nutr_No = 313
    LEFT JOIN [usda].NUT_DATA nd315 ON nd315.NDB_No = f.NDB_No AND nd315.Nutr_No = 315
    LEFT JOIN [usda].NUTR_DEF nf315 ON nf315.Nutr_No = 315
    LEFT JOIN [usda].NUT_DATA nd317 ON nd317.NDB_No = f.NDB_No AND nd317.Nutr_No = 317
    LEFT JOIN [usda].NUTR_DEF nf317 ON nf317.Nutr_No = 317
    LEFT JOIN [usda].NUT_DATA nd318 ON nd318.NDB_No = f.NDB_No AND nd318.Nutr_No = 318
    LEFT JOIN [usda].NUTR_DEF nf318 ON nf318.Nutr_No = 318
    LEFT JOIN [usda].NUT_DATA nd319 ON nd319.NDB_No = f.NDB_No AND nd319.Nutr_No = 319
    LEFT JOIN [usda].NUTR_DEF nf319 ON nf319.Nutr_No = 319
    LEFT JOIN [usda].NUT_DATA nd320 ON nd320.NDB_No = f.NDB_No AND nd320.Nutr_No = 320
    LEFT JOIN [usda].NUTR_DEF nf320 ON nf320.Nutr_No = 320
    LEFT JOIN [usda].NUT_DATA nd321 ON nd321.NDB_No = f.NDB_No AND nd321.Nutr_No = 321
    LEFT JOIN [usda].NUTR_DEF nf321 ON nf321.Nutr_No = 321
    LEFT JOIN [usda].NUT_DATA nd322 ON nd322.NDB_No = f.NDB_No AND nd322.Nutr_No = 322
    LEFT JOIN [usda].NUTR_DEF nf322 ON nf322.Nutr_No = 322
    LEFT JOIN [usda].NUT_DATA nd323 ON nd323.NDB_No = f.NDB_No AND nd323.Nutr_No = 323
    LEFT JOIN [usda].NUTR_DEF nf323 ON nf323.Nutr_No = 323
    LEFT JOIN [usda].NUT_DATA nd324 ON nd324.NDB_No = f.NDB_No AND nd324.Nutr_No = 324
    LEFT JOIN [usda].NUTR_DEF nf324 ON nf324.Nutr_No = 324
    LEFT JOIN [usda].NUT_DATA nd325 ON nd325.NDB_No = f.NDB_No AND nd325.Nutr_No = 325
    LEFT JOIN [usda].NUTR_DEF nf325 ON nf325.Nutr_No = 325
    LEFT JOIN [usda].NUT_DATA nd326 ON nd326.NDB_No = f.NDB_No AND nd326.Nutr_No = 326
    LEFT JOIN [usda].NUTR_DEF nf326 ON nf326.Nutr_No = 326
    LEFT JOIN [usda].NUT_DATA nd328 ON nd328.NDB_No = f.NDB_No AND nd328.Nutr_No = 328
    LEFT JOIN [usda].NUTR_DEF nf328 ON nf328.Nutr_No = 328
    LEFT JOIN [usda].NUT_DATA nd334 ON nd334.NDB_No = f.NDB_No AND nd334.Nutr_No = 334
    LEFT JOIN [usda].NUTR_DEF nf334 ON nf334.Nutr_No = 334
    LEFT JOIN [usda].NUT_DATA nd337 ON nd337.NDB_No = f.NDB_No AND nd337.Nutr_No = 337
    LEFT JOIN [usda].NUTR_DEF nf337 ON nf337.Nutr_No = 337
    LEFT JOIN [usda].NUT_DATA nd338 ON nd338.NDB_No = f.NDB_No AND nd338.Nutr_No = 338
    LEFT JOIN [usda].NUTR_DEF nf338 ON nf338.Nutr_No = 338
    LEFT JOIN [usda].NUT_DATA nd341 ON nd341.NDB_No = f.NDB_No AND nd341.Nutr_No = 341
    LEFT JOIN [usda].NUTR_DEF nf341 ON nf341.Nutr_No = 341
    LEFT JOIN [usda].NUT_DATA nd342 ON nd342.NDB_No = f.NDB_No AND nd342.Nutr_No = 342
    LEFT JOIN [usda].NUTR_DEF nf342 ON nf342.Nutr_No = 342
    LEFT JOIN [usda].NUT_DATA nd343 ON nd343.NDB_No = f.NDB_No AND nd343.Nutr_No = 343
    LEFT JOIN [usda].NUTR_DEF nf343 ON nf343.Nutr_No = 343
    LEFT JOIN [usda].NUT_DATA nd344 ON nd344.NDB_No = f.NDB_No AND nd344.Nutr_No = 344
    LEFT JOIN [usda].NUTR_DEF nf344 ON nf344.Nutr_No = 344
    LEFT JOIN [usda].NUT_DATA nd345 ON nd345.NDB_No = f.NDB_No AND nd345.Nutr_No = 345
    LEFT JOIN [usda].NUTR_DEF nf345 ON nf345.Nutr_No = 345
    LEFT JOIN [usda].NUT_DATA nd346 ON nd346.NDB_No = f.NDB_No AND nd346.Nutr_No = 346
    LEFT JOIN [usda].NUTR_DEF nf346 ON nf346.Nutr_No = 346
    LEFT JOIN [usda].NUT_DATA nd347 ON nd347.NDB_No = f.NDB_No AND nd347.Nutr_No = 347
    LEFT JOIN [usda].NUTR_DEF nf347 ON nf347.Nutr_No = 347
    LEFT JOIN [usda].NUT_DATA nd401 ON nd401.NDB_No = f.NDB_No AND nd401.Nutr_No = 401
    LEFT JOIN [usda].NUTR_DEF nf401 ON nf401.Nutr_No = 401
    LEFT JOIN [usda].NUT_DATA nd404 ON nd404.NDB_No = f.NDB_No AND nd404.Nutr_No = 404
    LEFT JOIN [usda].NUTR_DEF nf404 ON nf404.Nutr_No = 404
    LEFT JOIN [usda].NUT_DATA nd405 ON nd405.NDB_No = f.NDB_No AND nd405.Nutr_No = 405
    LEFT JOIN [usda].NUTR_DEF nf405 ON nf405.Nutr_No = 405
    LEFT JOIN [usda].NUT_DATA nd406 ON nd406.NDB_No = f.NDB_No AND nd406.Nutr_No = 406
    LEFT JOIN [usda].NUTR_DEF nf406 ON nf406.Nutr_No = 406
    LEFT JOIN [usda].NUT_DATA nd410 ON nd410.NDB_No = f.NDB_No AND nd410.Nutr_No = 410
    LEFT JOIN [usda].NUTR_DEF nf410 ON nf410.Nutr_No = 410
    LEFT JOIN [usda].NUT_DATA nd415 ON nd415.NDB_No = f.NDB_No AND nd415.Nutr_No = 415
    LEFT JOIN [usda].NUTR_DEF nf415 ON nf415.Nutr_No = 415
    LEFT JOIN [usda].NUT_DATA nd417 ON nd417.NDB_No = f.NDB_No AND nd417.Nutr_No = 417
    LEFT JOIN [usda].NUTR_DEF nf417 ON nf417.Nutr_No = 417
    LEFT JOIN [usda].NUT_DATA nd418 ON nd418.NDB_No = f.NDB_No AND nd418.Nutr_No = 418
    LEFT JOIN [usda].NUTR_DEF nf418 ON nf418.Nutr_No = 418
    LEFT JOIN [usda].NUT_DATA nd421 ON nd421.NDB_No = f.NDB_No AND nd421.Nutr_No = 421
    LEFT JOIN [usda].NUTR_DEF nf421 ON nf421.Nutr_No = 421
    LEFT JOIN [usda].NUT_DATA nd428 ON nd428.NDB_No = f.NDB_No AND nd428.Nutr_No = 428
    LEFT JOIN [usda].NUTR_DEF nf428 ON nf428.Nutr_No = 428
    LEFT JOIN [usda].NUT_DATA nd429 ON nd429.NDB_No = f.NDB_No AND nd429.Nutr_No = 429
    LEFT JOIN [usda].NUTR_DEF nf429 ON nf429.Nutr_No = 429
    LEFT JOIN [usda].NUT_DATA nd430 ON nd430.NDB_No = f.NDB_No AND nd430.Nutr_No = 430
    LEFT JOIN [usda].NUTR_DEF nf430 ON nf430.Nutr_No = 430
    LEFT JOIN [usda].NUT_DATA nd431 ON nd431.NDB_No = f.NDB_No AND nd431.Nutr_No = 431
    LEFT JOIN [usda].NUTR_DEF nf431 ON nf431.Nutr_No = 431
    LEFT JOIN [usda].NUT_DATA nd432 ON nd432.NDB_No = f.NDB_No AND nd432.Nutr_No = 432
    LEFT JOIN [usda].NUTR_DEF nf432 ON nf432.Nutr_No = 432
    LEFT JOIN [usda].NUT_DATA nd435 ON nd435.NDB_No = f.NDB_No AND nd435.Nutr_No = 435
    LEFT JOIN [usda].NUTR_DEF nf435 ON nf435.Nutr_No = 435
    LEFT JOIN [usda].NUT_DATA nd454 ON nd454.NDB_No = f.NDB_No AND nd454.Nutr_No = 454
    LEFT JOIN [usda].NUTR_DEF nf454 ON nf454.Nutr_No = 454
    LEFT JOIN [usda].NUT_DATA nd501 ON nd501.NDB_No = f.NDB_No AND nd501.Nutr_No = 501
    LEFT JOIN [usda].NUTR_DEF nf501 ON nf501.Nutr_No = 501
    LEFT JOIN [usda].NUT_DATA nd502 ON nd502.NDB_No = f.NDB_No AND nd502.Nutr_No = 502
    LEFT JOIN [usda].NUTR_DEF nf502 ON nf502.Nutr_No = 502
    LEFT JOIN [usda].NUT_DATA nd503 ON nd503.NDB_No = f.NDB_No AND nd503.Nutr_No = 503
    LEFT JOIN [usda].NUTR_DEF nf503 ON nf503.Nutr_No = 503
    LEFT JOIN [usda].NUT_DATA nd504 ON nd504.NDB_No = f.NDB_No AND nd504.Nutr_No = 504
    LEFT JOIN [usda].NUTR_DEF nf504 ON nf504.Nutr_No = 504
    LEFT JOIN [usda].NUT_DATA nd505 ON nd505.NDB_No = f.NDB_No AND nd505.Nutr_No = 505
    LEFT JOIN [usda].NUTR_DEF nf505 ON nf505.Nutr_No = 505
    LEFT JOIN [usda].NUT_DATA nd506 ON nd506.NDB_No = f.NDB_No AND nd506.Nutr_No = 506
    LEFT JOIN [usda].NUTR_DEF nf506 ON nf506.Nutr_No = 506
    LEFT JOIN [usda].NUT_DATA nd507 ON nd507.NDB_No = f.NDB_No AND nd507.Nutr_No = 507
    LEFT JOIN [usda].NUTR_DEF nf507 ON nf507.Nutr_No = 507
    LEFT JOIN [usda].NUT_DATA nd508 ON nd508.NDB_No = f.NDB_No AND nd508.Nutr_No = 508
    LEFT JOIN [usda].NUTR_DEF nf508 ON nf508.Nutr_No = 508
    LEFT JOIN [usda].NUT_DATA nd509 ON nd509.NDB_No = f.NDB_No AND nd509.Nutr_No = 509
    LEFT JOIN [usda].NUTR_DEF nf509 ON nf509.Nutr_No = 509
    LEFT JOIN [usda].NUT_DATA nd510 ON nd510.NDB_No = f.NDB_No AND nd510.Nutr_No = 510
    LEFT JOIN [usda].NUTR_DEF nf510 ON nf510.Nutr_No = 510
    LEFT JOIN [usda].NUT_DATA nd511 ON nd511.NDB_No = f.NDB_No AND nd511.Nutr_No = 511
    LEFT JOIN [usda].NUTR_DEF nf511 ON nf511.Nutr_No = 511
    LEFT JOIN [usda].NUT_DATA nd512 ON nd512.NDB_No = f.NDB_No AND nd512.Nutr_No = 512
    LEFT JOIN [usda].NUTR_DEF nf512 ON nf512.Nutr_No = 512
    LEFT JOIN [usda].NUT_DATA nd513 ON nd513.NDB_No = f.NDB_No AND nd513.Nutr_No = 513
    LEFT JOIN [usda].NUTR_DEF nf513 ON nf513.Nutr_No = 513
    LEFT JOIN [usda].NUT_DATA nd514 ON nd514.NDB_No = f.NDB_No AND nd514.Nutr_No = 514
    LEFT JOIN [usda].NUTR_DEF nf514 ON nf514.Nutr_No = 514
    LEFT JOIN [usda].NUT_DATA nd515 ON nd515.NDB_No = f.NDB_No AND nd515.Nutr_No = 515
    LEFT JOIN [usda].NUTR_DEF nf515 ON nf515.Nutr_No = 515
    LEFT JOIN [usda].NUT_DATA nd516 ON nd516.NDB_No = f.NDB_No AND nd516.Nutr_No = 516
    LEFT JOIN [usda].NUTR_DEF nf516 ON nf516.Nutr_No = 516
    LEFT JOIN [usda].NUT_DATA nd517 ON nd517.NDB_No = f.NDB_No AND nd517.Nutr_No = 517
    LEFT JOIN [usda].NUTR_DEF nf517 ON nf517.Nutr_No = 517
    LEFT JOIN [usda].NUT_DATA nd518 ON nd518.NDB_No = f.NDB_No AND nd518.Nutr_No = 518
    LEFT JOIN [usda].NUTR_DEF nf518 ON nf518.Nutr_No = 518
    LEFT JOIN [usda].NUT_DATA nd521 ON nd521.NDB_No = f.NDB_No AND nd521.Nutr_No = 521
    LEFT JOIN [usda].NUTR_DEF nf521 ON nf521.Nutr_No = 521
    LEFT JOIN [usda].NUT_DATA nd573 ON nd573.NDB_No = f.NDB_No AND nd573.Nutr_No = 573
    LEFT JOIN [usda].NUTR_DEF nf573 ON nf573.Nutr_No = 573
    LEFT JOIN [usda].NUT_DATA nd578 ON nd578.NDB_No = f.NDB_No AND nd578.Nutr_No = 578
    LEFT JOIN [usda].NUTR_DEF nf578 ON nf578.Nutr_No = 578
    LEFT JOIN [usda].NUT_DATA nd601 ON nd601.NDB_No = f.NDB_No AND nd601.Nutr_No = 601
    LEFT JOIN [usda].NUTR_DEF nf601 ON nf601.Nutr_No = 601
    LEFT JOIN [usda].NUT_DATA nd605 ON nd605.NDB_No = f.NDB_No AND nd605.Nutr_No = 605
    LEFT JOIN [usda].NUTR_DEF nf605 ON nf605.Nutr_No = 605
    LEFT JOIN [usda].NUT_DATA nd606 ON nd606.NDB_No = f.NDB_No AND nd606.Nutr_No = 606
    LEFT JOIN [usda].NUTR_DEF nf606 ON nf606.Nutr_No = 606
    LEFT JOIN [usda].NUT_DATA nd607 ON nd607.NDB_No = f.NDB_No AND nd607.Nutr_No = 607
    LEFT JOIN [usda].NUTR_DEF nf607 ON nf607.Nutr_No = 607
    LEFT JOIN [usda].NUT_DATA nd608 ON nd608.NDB_No = f.NDB_No AND nd608.Nutr_No = 608
    LEFT JOIN [usda].NUTR_DEF nf608 ON nf608.Nutr_No = 608
    LEFT JOIN [usda].NUT_DATA nd609 ON nd609.NDB_No = f.NDB_No AND nd609.Nutr_No = 609
    LEFT JOIN [usda].NUTR_DEF nf609 ON nf609.Nutr_No = 609
    LEFT JOIN [usda].NUT_DATA nd610 ON nd610.NDB_No = f.NDB_No AND nd610.Nutr_No = 610
    LEFT JOIN [usda].NUTR_DEF nf610 ON nf610.Nutr_No = 610
    LEFT JOIN [usda].NUT_DATA nd611 ON nd611.NDB_No = f.NDB_No AND nd611.Nutr_No = 611
    LEFT JOIN [usda].NUTR_DEF nf611 ON nf611.Nutr_No = 611
    LEFT JOIN [usda].NUT_DATA nd612 ON nd612.NDB_No = f.NDB_No AND nd612.Nutr_No = 612
    LEFT JOIN [usda].NUTR_DEF nf612 ON nf612.Nutr_No = 612
    LEFT JOIN [usda].NUT_DATA nd613 ON nd613.NDB_No = f.NDB_No AND nd613.Nutr_No = 613
    LEFT JOIN [usda].NUTR_DEF nf613 ON nf613.Nutr_No = 613
    LEFT JOIN [usda].NUT_DATA nd614 ON nd614.NDB_No = f.NDB_No AND nd614.Nutr_No = 614
    LEFT JOIN [usda].NUTR_DEF nf614 ON nf614.Nutr_No = 614
    LEFT JOIN [usda].NUT_DATA nd615 ON nd615.NDB_No = f.NDB_No AND nd615.Nutr_No = 615
    LEFT JOIN [usda].NUTR_DEF nf615 ON nf615.Nutr_No = 615
    LEFT JOIN [usda].NUT_DATA nd617 ON nd617.NDB_No = f.NDB_No AND nd617.Nutr_No = 617
    LEFT JOIN [usda].NUTR_DEF nf617 ON nf617.Nutr_No = 617
    LEFT JOIN [usda].NUT_DATA nd618 ON nd618.NDB_No = f.NDB_No AND nd618.Nutr_No = 618
    LEFT JOIN [usda].NUTR_DEF nf618 ON nf618.Nutr_No = 618
    LEFT JOIN [usda].NUT_DATA nd619 ON nd619.NDB_No = f.NDB_No AND nd619.Nutr_No = 619
    LEFT JOIN [usda].NUTR_DEF nf619 ON nf619.Nutr_No = 619
    LEFT JOIN [usda].NUT_DATA nd620 ON nd620.NDB_No = f.NDB_No AND nd620.Nutr_No = 620
    LEFT JOIN [usda].NUTR_DEF nf620 ON nf620.Nutr_No = 620
    LEFT JOIN [usda].NUT_DATA nd621 ON nd621.NDB_No = f.NDB_No AND nd621.Nutr_No = 621
    LEFT JOIN [usda].NUTR_DEF nf621 ON nf621.Nutr_No = 621
    LEFT JOIN [usda].NUT_DATA nd624 ON nd624.NDB_No = f.NDB_No AND nd624.Nutr_No = 624
    LEFT JOIN [usda].NUTR_DEF nf624 ON nf624.Nutr_No = 624
    LEFT JOIN [usda].NUT_DATA nd625 ON nd625.NDB_No = f.NDB_No AND nd625.Nutr_No = 625
    LEFT JOIN [usda].NUTR_DEF nf625 ON nf625.Nutr_No = 625
    LEFT JOIN [usda].NUT_DATA nd626 ON nd626.NDB_No = f.NDB_No AND nd626.Nutr_No = 626
    LEFT JOIN [usda].NUTR_DEF nf626 ON nf626.Nutr_No = 626
    LEFT JOIN [usda].NUT_DATA nd627 ON nd627.NDB_No = f.NDB_No AND nd627.Nutr_No = 627
    LEFT JOIN [usda].NUTR_DEF nf627 ON nf627.Nutr_No = 627
    LEFT JOIN [usda].NUT_DATA nd628 ON nd628.NDB_No = f.NDB_No AND nd628.Nutr_No = 628
    LEFT JOIN [usda].NUTR_DEF nf628 ON nf628.Nutr_No = 628
    LEFT JOIN [usda].NUT_DATA nd629 ON nd629.NDB_No = f.NDB_No AND nd629.Nutr_No = 629
    LEFT JOIN [usda].NUTR_DEF nf629 ON nf629.Nutr_No = 629
    LEFT JOIN [usda].NUT_DATA nd630 ON nd630.NDB_No = f.NDB_No AND nd630.Nutr_No = 630
    LEFT JOIN [usda].NUTR_DEF nf630 ON nf630.Nutr_No = 630
    LEFT JOIN [usda].NUT_DATA nd631 ON nd631.NDB_No = f.NDB_No AND nd631.Nutr_No = 631
    LEFT JOIN [usda].NUTR_DEF nf631 ON nf631.Nutr_No = 631
    LEFT JOIN [usda].NUT_DATA nd636 ON nd636.NDB_No = f.NDB_No AND nd636.Nutr_No = 636
    LEFT JOIN [usda].NUTR_DEF nf636 ON nf636.Nutr_No = 636
    LEFT JOIN [usda].NUT_DATA nd638 ON nd638.NDB_No = f.NDB_No AND nd638.Nutr_No = 638
    LEFT JOIN [usda].NUTR_DEF nf638 ON nf638.Nutr_No = 638
    LEFT JOIN [usda].NUT_DATA nd639 ON nd639.NDB_No = f.NDB_No AND nd639.Nutr_No = 639
    LEFT JOIN [usda].NUTR_DEF nf639 ON nf639.Nutr_No = 639
    LEFT JOIN [usda].NUT_DATA nd641 ON nd641.NDB_No = f.NDB_No AND nd641.Nutr_No = 641
    LEFT JOIN [usda].NUTR_DEF nf641 ON nf641.Nutr_No = 641
    LEFT JOIN [usda].NUT_DATA nd645 ON nd645.NDB_No = f.NDB_No AND nd645.Nutr_No = 645
    LEFT JOIN [usda].NUTR_DEF nf645 ON nf645.Nutr_No = 645
    LEFT JOIN [usda].NUT_DATA nd646 ON nd646.NDB_No = f.NDB_No AND nd646.Nutr_No = 646
    LEFT JOIN [usda].NUTR_DEF nf646 ON nf646.Nutr_No = 646
    LEFT JOIN [usda].NUT_DATA nd652 ON nd652.NDB_No = f.NDB_No AND nd652.Nutr_No = 652
    LEFT JOIN [usda].NUTR_DEF nf652 ON nf652.Nutr_No = 652
    LEFT JOIN [usda].NUT_DATA nd653 ON nd653.NDB_No = f.NDB_No AND nd653.Nutr_No = 653
    LEFT JOIN [usda].NUTR_DEF nf653 ON nf653.Nutr_No = 653
    LEFT JOIN [usda].NUT_DATA nd654 ON nd654.NDB_No = f.NDB_No AND nd654.Nutr_No = 654
    LEFT JOIN [usda].NUTR_DEF nf654 ON nf654.Nutr_No = 654
    LEFT JOIN [usda].NUT_DATA nd662 ON nd662.NDB_No = f.NDB_No AND nd662.Nutr_No = 662
    LEFT JOIN [usda].NUTR_DEF nf662 ON nf662.Nutr_No = 662
    LEFT JOIN [usda].NUT_DATA nd663 ON nd663.NDB_No = f.NDB_No AND nd663.Nutr_No = 663
    LEFT JOIN [usda].NUTR_DEF nf663 ON nf663.Nutr_No = 663
    LEFT JOIN [usda].NUT_DATA nd664 ON nd664.NDB_No = f.NDB_No AND nd664.Nutr_No = 664
    LEFT JOIN [usda].NUTR_DEF nf664 ON nf664.Nutr_No = 664
    LEFT JOIN [usda].NUT_DATA nd665 ON nd665.NDB_No = f.NDB_No AND nd665.Nutr_No = 665
    LEFT JOIN [usda].NUTR_DEF nf665 ON nf665.Nutr_No = 665
    LEFT JOIN [usda].NUT_DATA nd666 ON nd666.NDB_No = f.NDB_No AND nd666.Nutr_No = 666
    LEFT JOIN [usda].NUTR_DEF nf666 ON nf666.Nutr_No = 666
    LEFT JOIN [usda].NUT_DATA nd669 ON nd669.NDB_No = f.NDB_No AND nd669.Nutr_No = 669
    LEFT JOIN [usda].NUTR_DEF nf669 ON nf669.Nutr_No = 669
    LEFT JOIN [usda].NUT_DATA nd670 ON nd670.NDB_No = f.NDB_No AND nd670.Nutr_No = 670
    LEFT JOIN [usda].NUTR_DEF nf670 ON nf670.Nutr_No = 670
    LEFT JOIN [usda].NUT_DATA nd671 ON nd671.NDB_No = f.NDB_No AND nd671.Nutr_No = 671
    LEFT JOIN [usda].NUTR_DEF nf671 ON nf671.Nutr_No = 671
    LEFT JOIN [usda].NUT_DATA nd672 ON nd672.NDB_No = f.NDB_No AND nd672.Nutr_No = 672
    LEFT JOIN [usda].NUTR_DEF nf672 ON nf672.Nutr_No = 672
    LEFT JOIN [usda].NUT_DATA nd673 ON nd673.NDB_No = f.NDB_No AND nd673.Nutr_No = 673
    LEFT JOIN [usda].NUTR_DEF nf673 ON nf673.Nutr_No = 673
    LEFT JOIN [usda].NUT_DATA nd674 ON nd674.NDB_No = f.NDB_No AND nd674.Nutr_No = 674
    LEFT JOIN [usda].NUTR_DEF nf674 ON nf674.Nutr_No = 674
    LEFT JOIN [usda].NUT_DATA nd675 ON nd675.NDB_No = f.NDB_No AND nd675.Nutr_No = 675
    LEFT JOIN [usda].NUTR_DEF nf675 ON nf675.Nutr_No = 675
    LEFT JOIN [usda].NUT_DATA nd676 ON nd676.NDB_No = f.NDB_No AND nd676.Nutr_No = 676
    LEFT JOIN [usda].NUTR_DEF nf676 ON nf676.Nutr_No = 676
    LEFT JOIN [usda].NUT_DATA nd685 ON nd685.NDB_No = f.NDB_No AND nd685.Nutr_No = 685
    LEFT JOIN [usda].NUTR_DEF nf685 ON nf685.Nutr_No = 685
    LEFT JOIN [usda].NUT_DATA nd687 ON nd687.NDB_No = f.NDB_No AND nd687.Nutr_No = 687
    LEFT JOIN [usda].NUTR_DEF nf687 ON nf687.Nutr_No = 687
    LEFT JOIN [usda].NUT_DATA nd689 ON nd689.NDB_No = f.NDB_No AND nd689.Nutr_No = 689
    LEFT JOIN [usda].NUTR_DEF nf689 ON nf689.Nutr_No = 689
    LEFT JOIN [usda].NUT_DATA nd693 ON nd693.NDB_No = f.NDB_No AND nd693.Nutr_No = 693
    LEFT JOIN [usda].NUTR_DEF nf693 ON nf693.Nutr_No = 693
    LEFT JOIN [usda].NUT_DATA nd695 ON nd695.NDB_No = f.NDB_No AND nd695.Nutr_No = 695
    LEFT JOIN [usda].NUTR_DEF nf695 ON nf695.Nutr_No = 695
    LEFT JOIN [usda].NUT_DATA nd696 ON nd696.NDB_No = f.NDB_No AND nd696.Nutr_No = 696
    LEFT JOIN [usda].NUTR_DEF nf696 ON nf696.Nutr_No = 696
    LEFT JOIN [usda].NUT_DATA nd697 ON nd697.NDB_No = f.NDB_No AND nd697.Nutr_No = 697
    LEFT JOIN [usda].NUTR_DEF nf697 ON nf697.Nutr_No = 697
    LEFT JOIN [usda].NUT_DATA nd851 ON nd851.NDB_No = f.NDB_No AND nd851.Nutr_No = 851
    LEFT JOIN [usda].NUTR_DEF nf851 ON nf851.Nutr_No = 851
    LEFT JOIN [usda].NUT_DATA nd852 ON nd852.NDB_No = f.NDB_No AND nd852.Nutr_No = 852
    LEFT JOIN [usda].NUTR_DEF nf852 ON nf852.Nutr_No = 852
    LEFT JOIN [usda].NUT_DATA nd853 ON nd853.NDB_No = f.NDB_No AND nd853.Nutr_No = 853
    LEFT JOIN [usda].NUTR_DEF nf853 ON nf853.Nutr_No = 853
    LEFT JOIN [usda].NUT_DATA nd855 ON nd855.NDB_No = f.NDB_No AND nd855.Nutr_No = 855
    LEFT JOIN [usda].NUTR_DEF nf855 ON nf855.Nutr_No = 855
    LEFT JOIN [usda].NUT_DATA nd856 ON nd856.NDB_No = f.NDB_No AND nd856.Nutr_No = 856
    LEFT JOIN [usda].NUTR_DEF nf856 ON nf856.Nutr_No = 856
    LEFT JOIN [usda].NUT_DATA nd857 ON nd857.NDB_No = f.NDB_No AND nd857.Nutr_No = 857
    LEFT JOIN [usda].NUTR_DEF nf857 ON nf857.Nutr_No = 857
    LEFT JOIN [usda].NUT_DATA nd858 ON nd858.NDB_No = f.NDB_No AND nd858.Nutr_No = 858
    LEFT JOIN [usda].NUTR_DEF nf858 ON nf858.Nutr_No = 858
    LEFT JOIN [usda].NUT_DATA nd859 ON nd859.NDB_No = f.NDB_No AND nd859.Nutr_No = 859
    LEFT JOIN [usda].NUTR_DEF nf859 ON nf859.Nutr_No = 859
ORDER BY fd.FdGrp_Desc, f.Long_Desc