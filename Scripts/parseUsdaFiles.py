import sys, os

directory = sys.argv[1]
outdir = sys.argv[2] if len(sys.argv) > 2 else os.path.join(os.path.dirname(os.path.realpath(__file__)), "USDA_Parsed")
if not os.path.exists(outdir):
    os.makedirs(outdir)

print("The following files will be processed: {0}"
        .format((", ".join(file for file in os.listdir(directory) if os.path.splitext(file)[1] == ".txt"))))
print("=======================================")

for filename in os.listdir(directory):
    if os.path.splitext(filename)[1] != ".txt":
        print("Skipping non-text file {0}".format(filename))
        print("-------------------------------------------")
        continue
    else:
        print("Processing file {0}".format(filename))
    tablename = os.path.splitext(filename)[0]
    outfile = os.path.join(outdir, tablename+".txt")
    with open(directory + filename) as inf:
        total_lines = 0
        for line in inf:
            total_lines += 1
    with open(directory + filename) as inf, open(outfile,"w+") as outfile:
        filename = os.path.basename(filename)
        outfile.writelines("INSERT INTO [usda]." + 
            "[" + tablename + "] VALUES\n")
        lines = []
        for i, line in enumerate(inf, 1):
            if i % 1313 == 0 or total_lines - i < 1000:
                print("*** Processing line {0} of {1} ***".format(i, total_lines), end="\r")
            line = line.replace("\n", "")
            columns = line.split("^")
            columns = (column.replace("'", "''") for column in columns)
            columns = (column.replace("~", "'") for column in columns)
            newline = "(" + ",".join((column if len(column) > 0 else "NULL" for column in columns)) + ")"
            lines.append(newline)
            if i % 1000 == 0:
                outfile.writelines(",\n".join(lines))
                lines = []
                outfile.writelines("\nINSERT INTO [usda]." + 
                    "[" + tablename + "] VALUES\n")
        print(".....Saving new file.....                  ", end="\r")
        outfile.writelines(",\n".join(lines))
    print("File processed successfully!               ")
    print("-------------------------------------------")

print("Parsed files are saved at {0}".format(outdir))